//
//  WarungDetailVC.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

enum warungDetailContentType {
    case banner
    case description
    case header
}

class WarungDetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewButtomBar: UIView!
    @IBOutlet weak var buttonOrder: UIButton!
    @IBOutlet weak var buttonChat: UIButton!
    var warung : ShopViewData  = ShopViewData()
    var review : ShopReview?     = nil
    var interiors : [Interior]? = nil
    private var presenter : WarungDetailPresenter? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = self.warung.name
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        
        self.presenter = WarungDetailPresenter(data: self.warung)
        self.presenter?.attachView(view: self)
        self.presenter?.loadData()
        // Register Cell
        tableView.register(UINib(nibName: "WarungBanerCell", bundle: nil), forCellReuseIdentifier: "BanerCell")
        tableView.register(UINib(nibName: "WarungHeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "WarungDescriptionCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        tableView.register(UINib(nibName: "WarungFeatureCell", bundle: nil), forCellReuseIdentifier: "FeatureCell")
        tableView.register(UINib(nibName: "WarungFacilitiesCell", bundle: nil), forCellReuseIdentifier: "FacilitiyCell")
        tableView.register(UINib(nibName: "WarungReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        tableView.register(UINib(nibName: "WarungReviewHeaderCell", bundle: nil), forCellReuseIdentifier: "ReviewHeaderCell")
        
        tableView.estimatedRowHeight    = 134.5
        tableView.rowHeight             = UITableViewAutomaticDimension
        self.presenter?.clearOrder()
//        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func clickOrder(_ sender: Any) {
        let target  = FoodMenuVC()
        target.shopData = self.warung
        self.navigationController?.pushViewController(target, animated: true)
    }
    
    @IBAction func clickChat(_ sender: Any) {
        MainApp.shared.chat(withUser: self.warung.id, target: self)
    }
    
}

extension WarungDetailVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // facilities
        if indexPath.section == 1 {
            return 35 // Facility
        }
        if indexPath.section == 2 {
            return 35
        }
        if indexPath.section == 3 {
            return 110 // Review Cell
        }
    
        if indexPath.row == 0 {
            return 190 // Header
        }else if indexPath.row == 1 {
            return 140 // Detail
        }else if indexPath.row == 2 {
            return 180
        }else {
            return 0
        }
    }
}

extension WarungDetailVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // render facilities
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FacilitiyCell", for: indexPath) as! WarungFacilitiesCell
            let facility = self.warung.fasility[indexPath.row]
            cell.labelName.text = facility.name
            cell.imageAvatar.af_setImage(withURL: URL(string: facility.url)!, placeholderImage: UIImage(named: "halal_sign"), completion: nil)
            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewHeaderCell", for: indexPath) as! WarungReviewHeaderCell
            return cell
        }
        
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! WarungReviewCell
            if let data = self.review?.reviews[indexPath.row] {
                cell.labelName.text = data.orderName
                cell.setDetail(rating: Int(data.rating), date: data.date)
                cell.labelContent.text = data.content
                cell.imageAvatar.af_setImage(withURL: URL(string: data.orderImage)!, placeholderImage: UIImage(named: "profile"), completion: nil)
            }
            return cell
        }
        
        if indexPath.row    == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BanerCell", for: indexPath) as! WarungBanerCell
            cell.clear()
            if review != nil {
                cell.rating = (review?.rating)!
            }else {
                cell.rating = 0
            }
            // interior
            if let data = interiors {
                if data.count > 0 {
                    cell.imageViewBanner.af_setImage(withURL: URL(string: (data.first?.imageUrl)!)!, placeholderImage: UIImage(named: "logo"), completion: nil)
                }
            }
            return cell
        }else if indexPath.row    == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! WarungHeaderCell
            
            cell.labelName.text = self.warung.name
            cell.openHours  = self.warung.open
            cell.labelAddress.text = self.warung.address + ", " + self.warung.addressCity + ", " + self.warung.addressProvince
            cell.imageAvatar.af_setImage(withURL: URL(string: self.warung.imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
            cell.setup(target: self)
            return cell
//        }
//        else if indexPath.row    == 2 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! WarungDescriptionCell
//            cell.descriptionText = self.warung.note
//            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else if section == 1 {
            return self.warung.fasility.count
        }else if section == 2 {
            return 1
        }else if section == 3 {
            return self.review?.reviews.count ?? 0
        }else {
            return 0
        }
    }
}

extension WarungDetailVC : WarungDetailView {
    func setData(shop: ShopViewData) {
        //
    }
    
    func setData(review: ShopReview) {
        self.review = review
        self.tableView.reloadData()
    }
    
    func startLoading(message: String) {
        //
    }
    
    func finishLoading(message: String) {
        //
    }
    
    func setEmptyData(message: String) {
        //
    }
    
    func setData(interior: [Interior]) {
        self.interiors = interior
        self.tableView.reloadData()
    }
    
}
