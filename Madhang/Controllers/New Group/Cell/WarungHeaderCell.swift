//
//  WarungHeaderCell.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class WarungHeaderCell: UITableViewCell {

    @IBOutlet weak var labelOpen: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    var targetVC : UIViewController? = nil
    
    var openHours : String {
        set {
            self.labelOpen.text = "Open Now \(newValue)"
        }
        get {
            return openHours
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(target: UIViewController) {
        self.targetVC = target
        let tap = UITapGestureRecognizer(target: self, action: #selector(WarungHeaderCell.imageTapped(_:)))
        tap.delegate = self
        self.imageAvatar.addGestureRecognizer(tap)
        self.imageAvatar.isUserInteractionEnabled = true
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.targetVC?.view.addSubview(newImageView)
        self.targetVC?.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.targetVC?.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
}
