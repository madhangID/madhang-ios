//
//  WarungDescriptionCell.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class WarungDescriptionCell: UITableViewCell {

    @IBOutlet weak var TextViewDescription: UITextView!
    var descriptionText : String {
        set {
            self.TextViewDescription.text = "\(newValue)"
        }
        get {
            return descriptionText
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
