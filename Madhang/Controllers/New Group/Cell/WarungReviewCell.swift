//
//  WarungReviewCell.swift
//  Madhang
//
//  Created by asharijuang on 2/19/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class WarungReviewCell: UITableViewCell {

    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDetail(rating: Int, date: String) {
        var star = ""
        if rating > 0 {
            for _ in 1...rating {
                star = star + "⭐️"
            }
        }
        self.labelDetail.text = star + "  " + date
    }
    
}
