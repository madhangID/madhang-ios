//
//  WarungBanerCell.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class WarungBanerCell: UITableViewCell {

    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelRatingText: UILabel!
    @IBOutlet weak var imageViewBanner: UIImageView!
    var rating : Double {
        set {
            if (newValue != 0 && newValue <= 5) {
                for _ in 1...Int(ceil(newValue)) {
                    self.labelRating.text = self.labelRating.text! + "⭐️"
                }
            }else {
                self.labelRating.text = ""
            }
        }
        get {
            return self.rating
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func clear() {
        self.rating = 0.0
    }
}
