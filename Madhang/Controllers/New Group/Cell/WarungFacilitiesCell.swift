//
//  WarungFacilitiesCell.swift
//  Madhang
//
//  Created by asharijuang on 2/10/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class WarungFacilitiesCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
