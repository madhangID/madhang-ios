//
//  WarungDetailPresenter.swift
//  Madhang
//
//  Created by asharijuang on 2/19/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol WarungDetailView : BaseView {
    func setData(shop: ShopViewData)
    func setData(review: ShopReview)
    func setData(interior: [Interior])
}

class WarungDetailPresenter {
    private let service         : ShopService
    private var viewPresenter   : WarungDetailView?
    private var data            : ShopViewData
    init(data d: ShopViewData) {
        self.data = d
        self.service = ShopService()
    }
    
    func attachView(view : WarungDetailView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.setData(shop: self.data)
        self.loadReview()
        self.loadInterior()
    }
    
    private func loadReview() {
        service.loadReview(id: data.id) { (response) in
            switch(response){
            case .done(value: let result):
                self.viewPresenter?.setData(review: result)
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
        }
    }
    
    private func loadInterior() {
        service.loadInterior(id: data.id) { (response) in
            switch(response){
            case .done(value: let result):
                self.viewPresenter?.setData(interior: result)
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
        }
    }
    
    /**
     make sure old order already clear.
    */
    func clearOrder() {
        OrderManager.shared.clear()
    }
}
