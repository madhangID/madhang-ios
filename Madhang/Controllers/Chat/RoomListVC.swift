//
//  RoomListVC.swift
//  Example
//
//  Created by Ahmad Athaullah on 9/8/17.
//  Copyright © 2017 Ahmad Athaullah. All rights reserved.
//

import UIKit
import Qiscus

class RoomListVC: UITableViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var rooms = [QRoom]() {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chat List"
        self.tableView.register(UINib(nibName: "RoomListCell", bundle: nil), forCellReuseIdentifier: "roomCell")
        self.tableView.rowHeight = 63.0
        self.tableView.tableFooterView = UIView()
        Qiscus.chatDelegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden  = false
        self.rooms = QRoom.all()
        
        if self.rooms.count == 0 {
            self.loadRoomList()
        }else{
            Qiscus.subscribeAllRoomNotification()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.rooms.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "roomCell", for: indexPath) as! RoomListCell
        cell.room = self.rooms[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = self.rooms[indexPath.row]
        let chatView = Qiscus.chatView(withRoomId: room.id)
        self.tabBarController?.tabBar.isHidden  = true
        self.navigationController?.pushViewController(chatView, animated: true)
    }
    
    func loadRoomList(){
        Qiscus.fetchAllRoom(onSuccess: { (rooms) in
            self.rooms = rooms
            self.tableView.reloadData()
            Qiscus.subscribeAllRoomNotification()
            self.dismissQiscusLoading()
        }, onError: { (error) in
            print("error")
        }) { (progress, loadedRoom, totalRoom) in
            print("progress: \(progress) [\(loadedRoom)/\(totalRoom)]")
        }
    }
}

extension RoomListVC: QiscusChatDelegate {
    func qiscusChat(gotNewComment comment: QComment) {  self.rooms = QRoom.all() }
    func qiscusChat(gotNewRoom room: QRoom) { self.rooms = QRoom.all() }
}
