//
//  SearchFoodVC.swift
//  Madhang
//
//  Created by qiscus on 26/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchFoodVC: UIViewController {

    @IBOutlet weak var floatingCartHeight: NSLayoutConstraint!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelInformation: UILabel!
    @IBOutlet weak var floatingCart: FloatingCart!
    @IBOutlet weak var tableView: UITableView!
    internal let presenter : FoodSearchPresenter = FoodSearchPresenter()
    var foodData : [ShopViewData] = [ShopViewData]()
    var category : String   = ""
    var keywords : String   = ""
    var menuType : String   = "" // biasa, specialtoday, preorder
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Daftar Warung"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Register Cell
        tableView.register(UINib(nibName: "FoodCell", bundle: nil), forCellReuseIdentifier: "foodCell")
        tableView.estimatedRowHeight    = 134.5
        tableView.rowHeight             = UITableViewAutomaticDimension
        
        self.presenter.attachView(view: self)
        if !category.isEmpty {
            self.presenter.loadData(category: self.category)
        }
        if !keywords.isEmpty {
            self.presenter.loadData(keywords: self.keywords)
        }
        
        self.tabBarController?.tabBar.isHidden  = true
        // remove empty cell
        tableView.tableFooterView = UIView()
        self.loadingIndicator.hidesWhenStopped = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        floatingCart.setupUI(Controller: self)
//        if !OrderManager.shared.isCartActive() {
            self.floatingCart.isHidden          = true
            self.floatingCartHeight.constant    = 0
//        }else {
//            self.floatingCart.isHidden          = false
//            self.floatingCartHeight.constant    = 50
//        }
    }
}

extension SearchFoodVC : FoodSearchView {
    func setData(value: [ShopViewData]) {
        self.foodData = value
        self.tableView.reloadData()
    }
    
    func startLoading(message: String) {
        //
        self.loadingIndicator.startAnimating()
        self.tableView.isHidden = true
    }
    
    func finishLoading(message: String) {
        //
        self.loadingIndicator.stopAnimating()
        self.tableView.isHidden = false
    }
    
    func setEmptyData(message: String) {
        //
        self.loadingIndicator.stopAnimating()
        self.tableView.isHidden = true
        if !self.keywords.isEmpty {
            self.labelInformation.text  = "Maaf, Tidak ada makanan atau warung dengan nama \(self.keywords), Mohon coba lagi"
        }
        if !self.category.isEmpty {
            self.labelInformation.text  = "Maaf, Tidak ditemukan makanan atau warung dengan kategori tersebut"
        }
    }
}

extension SearchFoodVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134.5
    }
}

extension SearchFoodVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell", for: indexPath) as! FoodCell
        let data            = self.foodData[indexPath.row]
        cell.labelName.text = data.name
        cell.labelOpen.text = data.open
        cell.labelLocation.text = "\(data.distance) Km \(data.addressCity) \(data.addressProvince)"
        cell.isOpen         = data.isOpen
        cell.rating = Int(arc4random_uniform(6) + 1) // Random 1-5
        cell.review = Int(arc4random_uniform(20) + 10) // Random 1-5
        cell.imageAvatar.af_setImage(withURL: URL(string: data.imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
        cell.clearFacilities(data: self.presenter.loadFacilities())
        cell.setupFacilities(data: data.fasility)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data            = self.foodData[indexPath.row]
        let target = WarungDetailVC()
        target.warung   = data
        self.navigationController?.pushViewController(target, animated: true)
    }
}

