//
//  FoodCategoryCell.swift
//  Madhang
//
//  Created by qiscus on 26/11/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class FoodCategoryCell: UICollectionViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
