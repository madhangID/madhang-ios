//
//  FoodCategoriesVC.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

private let reuseIdentifier = "Cell"

class FoodCategoryVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private let presenter : FoodCategoryPresenter = FoodCategoryPresenter()
    var categoryData : [FoodCategoryViewData] = [FoodCategoryViewData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Register cell classes
        let nibCell = UINib(nibName: "FoodCategoryCell", bundle: nil)
        self.collectionView?.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsetsMake(
                Constant.offset,    // top
                Constant.offset,    // left
                Constant.offset,    // bottom
                Constant.offset     // right
            )
            
            layout.minimumInteritemSpacing = Constant.minItemSpacing
            layout.minimumLineSpacing = Constant.minLineSpacing
        }
        
        collectionView.isScrollEnabled = true
        // Do any additional setup after loading the view.
        self.presenter.attachView(view: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter.loadData()
    }

}

extension FoodCategoryVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FoodCategoryCell
        let data = self.categoryData[indexPath.row]
        cell.labelName.text = data.name
        cell.imageView.af_setImage(withURL: URL(string: data.imageUrl)!, placeholderImage: UIImage(named: "food"), completion: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.categoryData[indexPath.row]
        let target = SearchFoodVC()
        target.category = data.id
        self.navigationController?.pushViewController(target, animated: true)
    }
}

extension FoodCategoryVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = Constant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
}

extension FoodCategoryVC : FoodCategoryView {
    func setData(value: [FoodCategoryViewData]) {
        self.categoryData.removeAll() // clear previus data
        self.categoryData = value
        self.collectionView.reloadData()
    }
    
    func startLoading(message: String) {
        //
        if self.categoryData.isEmpty {
            self.collectionView.isHidden    = true
        }
    }
    
    func finishLoading(message: String) {
        //
        self.collectionView.isHidden    = false
    }
    
    func setEmptyData(message: String) {
        //
    }
}
