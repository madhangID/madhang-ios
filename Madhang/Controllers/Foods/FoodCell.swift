//
//  FoodCell.swift
//  Madhang
//
//  Created by qiscus on 26/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

class FoodCell: UITableViewCell {

    @IBOutlet weak var openIndicator: UIButton!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelReview: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelOpen: UILabel!
    @IBOutlet weak var labelName: UILabel!
    var review : Int {
        set {
            self.labelReview.text = "\(newValue) Review"
        }
        get {
            return self.review
        }
    }
    var rating : Int {
        set {
            if (newValue <= 5) {
                for _ in 1...newValue {
                    self.labelRating.text = self.labelRating.text! + "⭐️"
                }
            }else {
                self.labelRating.text = "⭐️⭐️⭐️⭐️⭐️"
            }
        }
        get {
            return self.rating
        }
    }
    
    var isOpen : Bool {
        set {
            if (newValue == true) {
                self.openIndicator.setTitle("Buka", for: UIControlState.normal)
                self.openIndicator.backgroundColor = UIColor.green
            }else {
                self.openIndicator.setTitle("Tutup", for: UIControlState.normal)
                self.openIndicator.backgroundColor = UIColor.red
            }
        }
        get {
            return self.isOpen
        }
    }
    @IBOutlet weak var viewFacilities: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupFacilities(data: [Facility]) {
        for (index,i) in data.enumerated() {
            //Image View
            let imageView = UIImageView()
            imageView.tag   = 111 + index
            imageView.heightAnchor.constraint(equalToConstant: self.viewFacilities.frame.height).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: self.viewFacilities.frame.height).isActive = true
            imageView.af_setImage(withURL: URL(string: i.url)!, placeholderImage: UIImage(named: "halal_sign"), completion: nil)
            viewFacilities.addArrangedSubview(imageView)
        }
        viewFacilities.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func clearFacilities(data: [Facility]) {
        for (index,_) in data.enumerated() {
            if let imageView = viewFacilities.viewWithTag(111 + index) as? UIImageView {
                imageView.removeFromSuperview()
            }
        }
    }
}
