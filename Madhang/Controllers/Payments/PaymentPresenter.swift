//
//  PaymentPresenter.swift
//  Madhang
//
//  Created by asharijuang on 3/14/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol PaymentView : BaseView {
    func updateSaldo(value: Int)
}

class PaymentPresenter {
    private let service : ProfileService
    private var viewPresenter : PaymentView?
    private var userProfile : Profile = Profile()
    var saldo : Int {
        get {
            return userProfile.deposite
        }
    }
    
    init() {
        self.service = ProfileService()
    }
    
    func attachView(view : PaymentView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.startLoading(message: "")
        service.checkProfile(phone: userProfile.phone) { (response) in
            self.viewPresenter?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result {
                    self.viewPresenter?.updateSaldo(value: self.userProfile.deposite)
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
        }
    }
}
