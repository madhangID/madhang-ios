//
//  PaymentsVC.swift
//  Madhang
//
//  Created by asharijuang on 2/18/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import MidtransKit

class PaymentsVC: UIViewController {

    var presenter : PaymentPresenter = PaymentPresenter()
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelSaldo: UILabel!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    var admin = 100
    var items = [20000,50000,100000,200000]
    var selectedIndex = 0
    var saldo : Int {
        set {
            self.labelSaldo.text = "Rp. \(newValue),-"
        }
        get {
            return self.saldo
        }
    }
    var total : Int {
        set {
            self.labelTotal.text = "Rp. \(newValue),-"
        }
        get {
            return self.total
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        presenter.loadData()
        self.title = "Madhang Pay"
        self.total = 0
        self.saldo = presenter.saldo
        self.tabBarController?.tabBar.isHidden  = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden  = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.tabBarController?.tabBar.isHidden  = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func selectTopUp(value: Int) {
        self.selectedIndex = value
        self.updateButtonUI(value: value)
        let ammount = items[value]
        self.total = ammount + admin
    }
    
    func updateButtonUI(value: Int) {
        let buttons = [self.button1,self.button2,self.button3,self.button4]
        for b in buttons {
            b?.backgroundColor = UIColor.white
            b?.setTitleColor(UIColor.darkGray, for: .normal)
        }
        // set selected ui
        buttons[value]?.backgroundColor = UIColor.orange
        buttons[value]?.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func clickTopUp(_ sender: Any) {
        
    }
    
    @IBAction func clickHistory(_ sender: Any) {
        
    }
    
    @IBAction func clickRedeem(_ sender: Any) {
        
    }
    
    @IBAction func clickTopup1(_ sender: Any) {
        self.selectTopUp(value: 0)
    }
    
    @IBAction func clickTopup2(_ sender: Any) {
        self.selectTopUp(value: 1)
    }
    
    @IBAction func clickTopup3(_ sender: Any) {
        self.selectTopUp(value: 2)
    }
    
    @IBAction func clickTopup4(_ sender: Any) {
        self.selectTopUp(value: 3)
    }
    
}

extension PaymentsVC : PaymentView {
    func updateSaldo(value: Int) {
        self.saldo = value
    }
    
    func startLoading(message: String) {
        self.loadingIndicator.startAnimating()
    }
    
    func finishLoading(message: String) {
        self.loadingIndicator.stopAnimating()
    }
    
    func setEmptyData(message: String) {
        
    }
    
    
}
