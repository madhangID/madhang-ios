//
//  CheckeKtpVC.swift
//  Madhang
//
//  Created by asharijuang on 4/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class CheckeKtpVC: UIViewController {
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    private let presenter : KtpPresenter = KtpPresenter()
    let profile = MainApp.shared.getProfile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)

        // Do any additional setup after loading the view.
        self.title = "Verifikasi eKTP"
        self.tabBarController?.tabBar.isHidden  = true
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        
        if !profile.verified && !profile.identityNo.isEmpty {
            self.labelNote.text = "Kamu Belum Mengirimkan data eKTP."
            self.buttonNext.setTitle("Kirim eKTP Sekarang", for: .normal)
        }else {
            self.labelNote.text = "Data KTP mu masih dalam peninjauan tim Madhang."
            self.buttonNext.setTitle("Back", for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickEndKTP(_ sender: Any) {
        if !profile.verified && !profile.identityNo.isEmpty {
            let target = NoeKtpVC()
            self.navigationController?.pushViewController(target, animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }

}

extension CheckeKtpVC : KtpView {
    func progressUpload(progress: Double) {
        //
    }
    
    func finishUpload(message: String, url: String) {
        //
    }
    
    func failedUpload(message: String) {
        //
    }
    
    func startLoading(message: String) {
        //
    }
    
    func finishLoading(message: String) {
        //
    }
    
    func setEmptyData(message: String) {
        //
    }
    
}
