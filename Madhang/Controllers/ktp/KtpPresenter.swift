//
//  KtpPresenter.swift
//  Madhang
//
//  Created by asharijuang on 4/12/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol KtpView : BaseView {
    func progressUpload(progress: Double)
    func finishUpload(message: String, url: String)
    func failedUpload(message: String)
}

class KtpPresenter {
    private let service         : ProfileService
    private var viewPresenter   : KtpView?
    private var profile : ProfileViewData?
    
    init() {
        self.service = ProfileService()
        loadData()
    }
    
    func attachView(view : KtpView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        let profile = MainApp.shared.getProfile()
        let result  = ProfileViewData()
        result.name = profile.name
        result.email    = profile.email
        result.phone    = profile.phone
        result.address  = profile.address
        result.city     = profile.city
        result.provinsi = profile.provinsi
        result.imageURL = profile.imageURL
        result.identityURL  = profile.identityURL
        result.verified = profile.verified
        
        self.profile = result
    }
    
    func isVerified() -> Bool {
        return self.profile?.verified ?? false
    }
    
    func save(no: String, ktp: String, ktpSelf: String) {
        self.service.changeKTP(no: no, ktp: ktp, ktpSelf: ktpSelf) { (response) in
            switch(response){
            case .done(value: (let ktp, let ktpSelf)):
                let data            = Profile()
                data.identityURL    = ktp
                data.identityNo     = no
                data.identitySelfURL = ktpSelf
                self.viewPresenter?.finishUpload(message: "", url: "")
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.failedUpload(message: message)
                break
            case .onProgress(progress: let value):
                self.viewPresenter?.progressUpload(progress: value)
                break
            }
            self.viewPresenter?.finishLoading(message: "")
        }
    }
    
}
