//
//  NoeKtpVC.swift
//  Madhang
//
//  Created by asharijuang on 4/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class NoeKtpVC: UIViewController {

    private let presenter : KtpPresenter = KtpPresenter()
    @IBOutlet weak var fieldNoKtp: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)

        // Do any additional setup after loading the view.
        self.title = "No eKTP"
        self.navigationController?.navigationBar.isHidden = false
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickNext(_ sender: Any) {
        guard let no = self.fieldNoKtp.text else {
            // show alert
            Helper.showError(message: "No KTP Harus di isi")
            return
        }
        let target = UploadeKtpVC()
        target.noKTP = no
        self.navigationController?.pushViewController(target, animated: true)
    }
    
}

extension NoeKtpVC : KtpView {
    func progressUpload(progress: Double) {
        //
    }
    
    func finishUpload(message: String, url: String) {
        //
    }
    
    func failedUpload(message: String) {
        //
    }
    
    func startLoading(message: String) {
        //
    }
    
    func finishLoading(message: String) {
        //
    }
    
    func setEmptyData(message: String) {
        //
    }
    
}
