//
//  UploadeKtpVC.swift
//  Madhang
//
//  Created by asharijuang on 4/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import ALCameraViewController

enum uploadKTP : String {
    case selfy
    case card
}

class UploadeKtpVC: UIViewController {

    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var imageKTP: UIImageView!
    var uploadType : uploadKTP = .card
    private let presenter : KtpPresenter = KtpPresenter()
    var urlimageKTP : String?
    var urlimageKTPSelf : String?
    var noKTP : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)

        // Do any additional setup after loading the view.
        if uploadType == .card {
            self.title = "Foto KTP"
            self.labelSubtitle.text = "Upload Foto KTP"
            self.imageKTP.image = UIImage(named: "upload_ktp")
            self.buttonNext.setTitle("Selanjutnya", for: .normal)
        }else {
            self.title = "Foto Diri"
            self.labelSubtitle.text = "Upload Foto Diri dengan KTP"
            self.imageKTP.image = UIImage(named: "upload_ktp_diri")
            self.buttonNext.setTitle("Simpan", for: .normal)
        }
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickNext(_ sender: Any) {
        let target : UploadeKtpVC = UploadeKtpVC()
        if self.uploadType == .card {
            target.uploadType = .selfy
            target.noKTP = self.noKTP
            target.urlimageKTP = self.urlimageKTP
            self.navigationController?.pushViewController(target, animated: true)
        }else {
            guard let ktpSelf = self.urlimageKTPSelf else {
                
                Helper.showError(message: "foto ktp dan foto diri harus di pilih")
                return
            }
            guard let ktp = self.urlimageKTP else {
                Helper.showError(message: "foto ktp harus dipilih")
                return
            }
            guard let no = self.noKTP else {
                Helper.showError(message: "no ktp harus di pilih")
                return
            }
            self.presenter.save(no: no, ktp: ktp, ktpSelf: ktpSelf)
        }
        
    }
    
    @IBAction func clickChangeKTP(_ sender: Any) {
        let cameraVC = CameraViewController(allowsLibraryAccess: true, allowsSwapCameraOrientation: true, allowVolumeButtonCapture: true) { (pickedImage, assets) in
            //
            if let image = pickedImage {
                self.imageKTP.image = image
                var prefix = "_ktp"
                if self.uploadType == .selfy {
                    prefix = "_ktp&selfy"
                }
                let resizeImage = self.resizeImage(image: image, targetSize: CGSize(width: 800, height: 600))
                let imagePath = UIImage.uploadImagePreparation(pickedImage: resizeImage, name: Profile().id + prefix)
                print("uploadTheAvatar \(imagePath)")
                if self.uploadType == .card {
                    self.urlimageKTP = imagePath
                }else {
                    self.urlimageKTPSelf = imagePath
                }
            }
            self.dismiss(animated: true, completion: nil)
        }
        
        present(cameraVC, animated: true, completion: nil)
    }

    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension UploadeKtpVC : KtpView {
    func progressUpload(progress: Double) {
        self.progressBar.progress = Float(progress)
    }
    
    func finishUpload(message: String, url: String) {
        self.progressBar.progress = 0.0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func failedUpload(message: String) {
        //
    }
    
    func startLoading(message: String) {
        //
    }
    
    func finishLoading(message: String) {
        //
    }
    
    func setEmptyData(message: String) {
        //
    }
}
