//
//  LoginVC.swift
//  Madhang
//
//  Created by asharijuang on 12/27/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AccountKit

class LoginVC: UIViewController {
    
    var accountKit: AKFAccountKit!
    var tapDemo : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // initialize Account Kit
        if accountKit == nil {
            // may also specify AKFResponseTypeAccessToken
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tapDemo = 0
        if let token = accountKit.currentAccessToken {
            // if the user is already logged in, go to the main screen
            print("User already logged in go to ViewController")
            MainApp.shared.getPhone(accessToken: token.tokenString) { (phone) in
                MainApp.shared.validateUser()
            }
        }
    }
    @IBAction func clickPhoneNumber(_ sender: Any) {
//
        let inputState: String = UUID().uuidString
        let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState)  as AKFViewController
        viewController.enableSendToFacebook = true
        self.prepareLoginViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func loginWithPhone(_ sender: AnyObject) {
        //login with Phone
        let inputState: String = UUID().uuidString
        let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState)  as AKFViewController
        viewController.enableSendToFacebook = true
        self.prepareLoginViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }

    @IBAction func clickDemo(_ sender: Any) {
        self.tapDemo += 1
        if self.tapDemo == 7 {
            self.tapDemo = 0
            Helper.showInputDialog(target: self, title: "Demo Account", subtitle: "Please input demo number", actionTitle: "Send", cancelTitle: "Cancel", inputPlaceholder: "Demo Number", inputKeyboardType: UIKeyboardType.numberPad, cancelHandler: nil, actionHandler: { (number) in
                if let phone = number {
                    MainApp.shared.demoUser(phone: phone)
                }
            })
        }
    }
    
    
}

extension LoginVC : AKFViewControllerDelegate {
    func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        MainApp.shared.getPhone(accessToken: accessToken.tokenString) { (phone) in
            MainApp.shared.validateUser()
        }
    }
    func viewController(_ viewController: UIViewController!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("Login succcess with AuthorizationCode")
        MainApp.shared.validateUser()
    }
    private func viewController(_ viewController: UIViewController!, didFailWithError error: NSError!) {
        print("We have an error \(error)")
    }
    func viewControllerDidCancel(_ viewController: UIViewController!) {
        print("The user cancel the login")
    }
    
    func prepareLoginViewController(_ loginViewController: AKFViewController) {
        
        loginViewController.delegate = self
        loginViewController.uiManager   = AKFSkinManager.init(skinType: AKFSkinType.contemporary, primaryColor: UIColor.orange)
        loginViewController.setAdvancedUIManager(nil)
        loginViewController.whitelistedCountryCodes = ["ID"]
        loginViewController.defaultCountryCode  = "ID"
        
        //Costumize the theme
        let theme:AKFTheme = AKFTheme.default()
        theme.headerBackgroundColor = Helper.orange()
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = Helper.orange()
        theme.inputTextColor = UIColor(white: 0.4, alpha: 1.0)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        theme.inputStyle = .underline
        theme.buttonBackgroundColor = Helper.orange()
        theme.inputBackgroundColor  = UIColor.clear
        theme.buttonBorderColor = Helper.orange()
        theme.buttonTextColor   = UIColor.white
        
        loginViewController.setTheme(theme)

    }
}
