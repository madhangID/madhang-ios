//
//  CartDetailCell.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class CartDetailCell: UITableViewCell {

    var controller: UIViewController?
    
    @IBOutlet weak var buttonCoupon: UIButton!
    @IBOutlet weak var fieldCoupon: UITextField!
    @IBOutlet weak var labelTextTotal: UILabel!
    @IBOutlet weak var labelTextSubTotal: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelSubtotal: UILabel!
    var submitOrder : () -> Void = {}
    var checkCoupon : (String) -> Void = { _ in}
    
    var total: String {
        set {
            self.labelTotal.text    = newValue
        }
        get {
            return self.total
        }
    }
    var discount: String {
        set {
            self.labelDiscount.text    = newValue
        }
        get {
            return self.discount
        }
    }
    var subTotal: String {
        set {
            self.labelSubtotal.text    = newValue
        }
        get {
            return self.subTotal
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI(controller: UIViewController) {
        self.controller = controller
    }
    
    @IBAction func clickProceedOrder(_ sender: Any) {
        self.submitOrder()
    }
    @IBAction func clickCoupon(_ sender: Any) {
        if let data = self.fieldCoupon.text { self.checkCoupon(data)
        }
    }
}
