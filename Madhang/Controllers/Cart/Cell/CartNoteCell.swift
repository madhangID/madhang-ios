//
//  CartNoteCell.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class CartNoteCell: UITableViewCell {

    @IBOutlet weak var fieldNote: UITextView!
    let placeholder : String = "Tambah Catatan"
    var note        : String {
        get {
            return OrderManager.shared.getNote()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.fieldNote.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPlaceholder(_ value: Bool = true) {
        if self.note.isEmpty && value {
            self.fieldNote.text = self.placeholder
            self.fieldNote.textColor    = UIColor.gray
        }else {
            self.fieldNote.text = ""
            self.fieldNote.textColor    = UIColor.darkGray
        }
    }
}

extension CartNoteCell : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        // empty input
        self.setPlaceholder(false)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if !textView.text.isEmpty {
            OrderManager.shared.setNote(value: textView.text)
        }else {
            self.setPlaceholder()
        }
    }
}
