//
//  CartDateCell.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import DateTimePicker

class CartDateCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var labelOrderMenu: UILabel!
    @IBOutlet weak var labelDeliveryMethod: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelNumberGuest: UILabel!
    @IBOutlet weak var fieldDate: UITextField!
    @IBOutlet weak var fieldNumberGuest: UITextField!
    @IBOutlet weak var switchCod: UISegmentedControl!
    
    let manager = OrderManager.shared
    var selectedDate : Date = Date()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let today = Date()
        selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.fieldNumberGuest.delegate = self
    }
    
    @IBAction func datePicker(_ sender: Any) {
        let picker = DateTimePicker.show()
        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        
        picker.selectedDate = selectedDate
        picker.isDatePickerOnly = true // to hide time and show only date picker
        picker.locale           = Locale.init(identifier: "id_ID")
        picker.completionHandler = { date in
            // validate tomorrow
            if !self.validateDate(date: date) {
                self.makeToast("Maaf, Pemesanan minimal dapat dilakukan untuk 1 hari berikutnya")
                self.fieldDate.text = ""
                self.fieldDate.placeholder = "-"
                return
            }
            
            // do something after tapping done
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date
            formatter.dateFormat = "yyyy-MM-dd"
            
            let myString = formatter.string(from: date)
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat    = "dd/MM/yyyy"
            // again convert your date to string
            let dateOrder                = formatter.string(from: yourDate!)
            print("data : \(date)")
            // MARK: TODO refactor leter this not MVP
            self.selectedDate = date
            self.manager.setOrderDate(date: date)
            self.fieldDate.text = "\(dateOrder)"
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text   != "" {
            self.manager.setOrderGuest(number: Int(textField.text!)!)
        }
    }
    @IBAction func timePicker(_ sender: Any) {
        let picker = DateTimePicker.show()
        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        picker.selectedDate = selectedDate
        picker.isTimePickerOnly = true
        picker.locale           = Locale.init(identifier: "id_ID")
        picker.completionHandler = { date in
            // do something after tapping done
            let formatter = DateFormatter()
            formatter.dateFormat    = "HH:mm"
            let time     = formatter.string(from: date)
            print("time : \(time)")
            // MARK: TODO refactor leter this not MVP
            
            self.manager.setOrderTime(date: date)
            self.fieldNumberGuest.text = "\(time)"
            self.selectedDate = date
        }
    }
    
    func validateDate(date: Date) -> Bool {
        if Date() < date {
            return true // x day ago
        }else {
            return false // tomorrow or in x days
        }
    }
}
