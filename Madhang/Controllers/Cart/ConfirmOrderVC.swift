//
//  ConfirmOrderVC.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage
import UnderKeyboard

class ConfirmOrderVC: UIViewController {

    @IBOutlet weak var buttomConstraint: NSLayoutConstraint!
    let underKeyboardLayoutConstraint = UnderKeyboardLayoutConstraint()
    @IBOutlet weak var tableview: UITableView!
    let presenter   : CartPresenter     = CartPresenter()
    var orderData   : [FoodViewData]    = [FoodViewData]()
    var order       : Order             = Order()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Konfirmasi Order"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Register Cell
        self.tableview.register(UINib(nibName: "CartDateCell", bundle: nil), forCellReuseIdentifier: "DateCell")
        self.tableview.register(UINib(nibName: "CartDetailCell", bundle: nil), forCellReuseIdentifier: "DetailCell")
        self.tableview.register(UINib(nibName: "CartNoteCell", bundle: nil), forCellReuseIdentifier: "NoteCell")
        self.tableview.register(UINib(nibName: "FoodMenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        
        //
        self.presenter.attachView(view: self)
        self.presenter.loadData()
        self.hideKeyboardWhenTappedAround()
        underKeyboardLayoutConstraint.setup(buttomConstraint, view: view)
    }

    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            self.presenter.clear()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ConfirmOrderVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section    == 0 {
            return 205
        }else if indexPath.section    == 1 {
            return 100
        }else if indexPath.section    == 2 {
            if indexPath.row    == 0 {
                return 100
            }else if indexPath.row    == 1 {
                return 250
            }
        }
        return 0.0
    }
}

extension ConfirmOrderVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            // Order items count
            return self.orderData.count
        }else if section == 2 {
            return 2
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section    == 0 {
            if indexPath.row    == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DateCell", for: indexPath) as! CartDateCell
                
                return cell
            }else {
                return UITableViewCell()
            }
        }else if indexPath.section    == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! FoodMenuCell
            let foodData    = self.orderData[indexPath.row]
            cell.labelName.text     = foodData.name
            cell.labelPrice.text    = String(foodData.price)
            cell.labelDescription.text  = foodData.desc
            cell.imageFood.af_setImage(withURL: URL(string: foodData.imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
            cell.foodData           = foodData
            cell.orderCount         = foodData.total
            cell.buttonCart.isHidden = true
            cell.setupUI(target: self)
            
            return cell
        }else if indexPath.section    == 2 {
            if indexPath.row  == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath) as! CartNoteCell
                return cell
            }else if indexPath.row  == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! CartDetailCell
                cell.total      = self.presenter.total()
                cell.subTotal   = self.presenter.subTotal()
                cell.discount   = self.presenter.discount()
                cell.setupUI(controller: self)
                cell.submitOrder    = { 
                    self.submitOrder()
                }
                cell.checkCoupon = { voucher in
                    self.presenter.chechVoucher(code: voucher)
                }
                return cell
            }else {
                return UITableViewCell()
            }
        }else {
            return UITableViewCell()
        }
    }
    
    func submitOrder() {
        self.order.note = ""
        self.order.delivery     = .cod
        self.order.payment      = .cash
        self.presenter.preccess(order: self.order)
    }
}

extension ConfirmOrderVC : CartView {
    func startCoupon(message: String) {
        Helper.showLoading(message: message)
    }
    
    func finishCoupon(message: String) {
        Helper.showFlash(message: message)
        self.tableview.reloadData()
    }
    
    func startOrder(message: String) {
        Helper.showLoading(message: message)
    }
    
    func finishOrder(message: String) {
        Helper.dissmissLoading()
        let target      = OrderResultVC()
        target.order    = self.order
        self.navigationController?.pushViewController(target, animated: true)
    }
    
    func failedOrder(message: String) {
        Helper.showError(message: message)
    }
    
    func setData(value: [FoodViewData]) {
        self.orderData = value
        self.tableview.reloadData()
    }
    
    func startLoading(message: String) {
        //
    }
    
    func finishLoading(message: String) {
        //
    }
    
    func setEmptyData(message: String) {
        //
    }
    
    func willUpdate() {
        self.tableview.reloadSections([2], with: .automatic)
    }
}
