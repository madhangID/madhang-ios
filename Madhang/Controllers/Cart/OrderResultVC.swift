//
//  OrderResultVC.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class OrderResultVC: UIViewController {
    var order       : Order? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Success Order"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden   = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickChat(_ sender: Any) {
        if let data = self.order {
            MainApp.shared.chat(withUser: data.id, target: self)
        }
    }
    
    @IBAction func clickDetailOrder(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func clickDismiss(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
