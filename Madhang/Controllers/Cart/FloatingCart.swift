//
//  FloatingCart.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

protocol FloatingCartDelegate {
    func didUpdate()
//    func didDisappear()
//    func didApprear()
}

class FloatingCart: UIView {
    @IBOutlet weak var labelPriceText: UILabel!
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var labelPrice: UILabel!
    var viewController : UIViewController?
    var delegate : FloatingCartDelegate? = nil
    
    private var price : Int {
        set {
            self.labelPrice.text    = "Rp. \(newValue)"
        }
        get {
            return self.price
        }
    }
    private var manager : OrderManager  = OrderManager.shared
    
    // If someone is to initialize a WeatherView in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // If someone is to initalize a WeatherView in Storyboard setting the Custom Class of a UIView
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // 1. Load the nib named 'WeatherView' into memory, finding it in the main bundle.
        Bundle.main.loadNibNamed("FloatingCart", owner: self, options: [:])
        
        // 2. Adding the 'contentView' to self (self represents the instance of a WeatherView which is a 'UIView').
        addSubview(contentView)
        
        // 3. Setting this false allows us to set our constraints on the contentView programtically
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        // 4. Setting the constraints programatically
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    func setupUI(Controller: UIViewController) {
        self.viewController = Controller
        self.price  = manager.totalPrice
        // cart update
        self.manager.update = { _ in
            self.price  = self.manager.totalPrice
            self.delegate?.didUpdate()
        }
    }
    
    @IBAction func clickCart(_ sender: Any) {
        let target = ConfirmOrderVC()
        self.viewController?.navigationController?.pushViewController(target, animated: true)
    }
    
}
