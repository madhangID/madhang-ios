//
//  SettingVC.swift
//  qisme
//
//  Created by qiscus on 1/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import AlamofireImage

class SettingVC: UIViewController {

    @IBOutlet weak var profileAvatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundProfileView: UIView!
    let profile = MainApp.shared.getProfile()

    internal let menus: [String] = ["Verifikasi KTP","Pemberitahuan", "Bagikan", "Bantuan", "Info"]
    internal let icons: [String] = ["ic_card","notif", "share", "help", "information"]
    
    internal let menus2: [String] = ["Logout"]
    internal let icons2: [String] = ["help"]

    
    // MARK: - Variable for SettingProfile & Avatar
    internal var avatarURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Profile"
        // Do any additional setup after loading the view.

        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden  = false
        self.tableView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @objc func changeAvatarIsSucceed(_ sender: Notification) {
        DispatchQueue.main.async(execute: {

        })
    }
    
    @objc func changeProfileIsSucceed(_ sender: Notification) {
        DispatchQueue.main.async(execute: {

        })
    }
    
    private func setupUI() {
        self.title = "Settings"
        
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        
        // MARK: - Register table & cell
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCellIdentifier")
        self.tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "profileCell")
    }
   
    func isVerified() -> Bool {
        return profile.verified
    }
    
}

extension SettingVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            // edit profile
            self.updateProfile()
        }else if indexPath.section == 1 {
//            if indexPath.row    == 0 {
//                let target = PaymentsVC()
//                self.navigationController?.pushViewController(target, animated: true)
//            }else
            if indexPath.row    == 0 {
                let target = CheckeKtpVC()
                self.navigationController?.pushViewController(target, animated: true)
            }else if indexPath.row    ==  1 {
                let target = AnouncementVC()
                self.navigationController?.pushViewController(target, animated: true)
            }else if indexPath.row    ==  2 {
                self.share()
            }else if indexPath.row    == 3 {
                self.open(url: "https://madhang.id/faq")
            }else if indexPath.row    == 4 {
                let target = AboutVC()
                self.navigationController?.pushViewController(target, animated: true)
            }
        }else if indexPath.section    == 2 {
            // logout
            self.confirmLogout()
        }
    }
}

extension SettingVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return self.menus.count
        }else if section == 2 {
            return self.menus2.count
        }else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90.0
        }else {
            if indexPath.section == 1 && indexPath.row == 0 {
                if self.isVerified() {
                    return 0
                }else {
                    return 50
                }
            }else {
                return 50.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileCell
            
            cell.labelName.text = profile.name
            cell.avatarImageView.af_setImage(withURL: URL(string: profile.imageURL)!, placeholderImage: UIImage(named: "profile"), completion: nil)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellIdentifier", for: indexPath) as! SettingCell
        cell.isDiclosureIndicatorNeeded(isNeeded: false)
        cell.iconWidth  = cell.iconHeight
        cell.menuLabel.textAlignment    = .left
        cell.labelLeftMargin.constant   = 15
        var iconImage: UIImage  = UIImage()

        if indexPath.section == 1 {
            iconImage = UIImage(named: self.icons[indexPath.row])!
            cell.menuLabel.text = self.menus[indexPath.row]
            cell.iconImageView.isHidden = false
            cell.iconImageViewWidth.constant    = 24
            if self.isVerified() && indexPath.row == 0 {
                cell.menuLabel.isHidden = true
                cell.iconImageView.isHidden = true
            }else {
                cell.menuLabel.isHidden = false
                cell.iconImageView.isHidden = false
            }
        }else if indexPath.section == 2 {
            iconImage = UIImage(named: self.icons2[indexPath.row])!
            cell.menuLabel.text = self.menus2[indexPath.row]
            cell.accessoryType = .none
            cell.labelLeftMargin.constant   = 0
            cell.menuLabel.textAlignment    = .center
            cell.menuLabel.textColor    = UIColor.red
            cell.iconImageViewWidth.constant    = 0
        }
        cell.iconImageView.image = iconImage
        return cell
    }
    
}

extension SettingVC {

    func confirmLogout() {
        let alert = UIAlertController(title: "Konfirmasi", message: "Apakah anda yakin ingin logout?", preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Logout", style: .default, handler: { response in
            self.logout()
        })
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { response in
            print("You're click cancel button. Response: \(response)")
        })

        alert.addAction(okButton)
        alert.addAction(cancelButton)

        self.present(alert, animated: true, completion: nil)
    }
    
    func logout() {
        MainApp.shared.logout()
    }
    
    func updateProfile() {
        let target = ProfileVC()
        target.isUpdate = true
        self.navigationController?.pushViewController(target, animated: true)
    }
    
    func share() {
        let firstActivityItem = "Yuk cari warung di Madhang, dan dapatkan aplikasinya di"
        let secondActivityItem : NSURL = NSURL(string: "https://madhang.id/")!
        // If you want to put an image
        let image : UIImage = UIImage(named: "logo_madhang")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func open(url: String) {
        guard let url = URL(string: url) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
