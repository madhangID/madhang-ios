//
//  SettingPresenter.swift
//  qisme
//
//  Created by qiscus on 1/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import Foundation
import UIKit

protocol SettingCommand {
    func loadProfile()
    func cellTableDidTap(index: IndexPath)
    func turnOfNotification(isOn: Bool)
}

protocol UISettingViewDelegate {
//    func loadProfileDidSucceed(result: MyProfile)
//    func loadProfileDidFailed(message: String)
    
    func cellProfileDidTap()
    func cellTermConditionDidTap()
    func cellAboutDidTap()
    func cellLogoutDidTap()
    func cellTurnOffNotifDidTap()
}

class SettingPresenter: SettingCommand {
    private var delegate: UISettingViewDelegate
    
    init(withDelegate: UISettingViewDelegate) {
        self.delegate = withDelegate
    }
    
    func loadProfile() {
    }
    
    internal func cellTableDidTap(index: IndexPath) {
        if index.section == 0 {
            self.delegate.cellProfileDidTap()
        }else if index.section == 1 {
            self.delegate.cellTurnOffNotifDidTap()
        }else if index.section == 2 {
            switch(index.row) {
            case 0:
                self.delegate.cellAboutDidTap()
                break
            case 1:
                self.delegate.cellTermConditionDidTap()
                break
            default:
                break
            }
        }else if index.section == 3 {
            self.delegate.cellLogoutDidTap()
        }
    }
    
    internal func turnOfNotification(isOn: Bool) {
        
    }
    
}
