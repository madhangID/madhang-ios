//
//  ProfileCell.swift
//  Main
//
//  Created by qiscus on 07/12/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import AlamofireImage

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    let profile : Profile = Profile()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelName.textAlignment     = NSTextAlignment.left
        labelName.textColor         = UIColor.darkText
        labelName.font              = UIFont.systemFont(ofSize: 14)
        labelName.text              = profile.name
        
        let iconLayer: CALayer?     = avatarImageView.layer
        let imageRadius: CGFloat    = CGFloat(avatarImageView!.frame.size.height / 1.4)
        iconLayer!.cornerRadius     = imageRadius
        iconLayer!.masksToBounds    = true
        
        avatarImageView.af_setImage(withURL: URL(string: profile.imageURL)!, placeholderImage: UIImage(named: "profile"), completion: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
