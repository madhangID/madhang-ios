//
//  AboutVC.swift
//  qisme
//
//  Created by qiscus on 1/18/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
        self.tabBarController?.tabBar.isHidden  = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupUI() {
        self.title = "Tentang Aplikasi"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
    }
    
    @IBAction func clickYoutube(_ sender: Any) {
        self.open(url: "http://bit.ly/madhang_youtube")
    }
    
    @IBAction func clickFacebook(_ sender: Any) {
         self.open(url: "http://m.facebook.com/madhang.id")
    }
    
    @IBAction func clickTwitter(_ sender: Any) {
        self.open(url: "http://twitter.com/madhang.id")
    }
    
    @IBAction func clickInstagram(_ sender: Any) {
        self.open(url: "http://instagram.com/madhang.id")
    }
    
    func open(url: String) {
        guard let url = URL(string: url) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
