//
//  SettingCell.swift
//  qisme
//
//  Created by qiscus on 1/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit

protocol SettingDelegate {
    func onSwitchChanged(isOn: Bool)
}

class SettingCell: UITableViewCell {

    @IBOutlet weak var iconImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var labelLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var iconHeight: NSLayoutConstraint!
    @IBOutlet weak var iconWidth: NSLayoutConstraint!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var switchStatus: UISwitch!
    
    var delegate: SettingDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func isDiclosureIndicatorNeeded(isNeeded: Bool) {
        if isNeeded {
            self.accessoryType = .none
            self.switchStatus.isHidden = false
        } else {
            self.accessoryType = .disclosureIndicator
            self.switchStatus.isHidden = true
        }
    }
    
    @IBAction func switchChangeListener(_ sender: Any) {
        print("switch state \(self.switchStatus.isOn)")
        if delegate != nil {
            self.delegate?.onSwitchChanged(isOn: switchStatus.isOn)
        }
    }
    
}
