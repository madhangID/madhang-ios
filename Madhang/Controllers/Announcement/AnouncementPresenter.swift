//
//  AnouncementPresenter.swift
//  Madhang
//
//  Created by asharijuang on 3/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol AnouncementView : BaseView {
    func setData(value: [Anouncement])
}

class AnouncementPresenter {
    private let service : ToolService
    private var viewPresenter : AnouncementView?
    
    init() {
        self.service = ToolService()
    }
    
    func attachView(view : AnouncementView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.startLoading(message: "")
        self.service.anouncement { (response) in
            switch response {
            case .done(let result):
                self.viewPresenter?.setData(value: result)
                break
            case .failed(let message):
                self.viewPresenter?.setEmptyData(message: message)
                break
            default: break
            }
            self.viewPresenter?.finishLoading(message: "")
        }
    }
    
}
