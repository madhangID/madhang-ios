//
//  AnouncementVC.swift
//  Madhang
//
//  Created by asharijuang on 3/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class AnouncementVC: UITableViewController {
    
    internal let presenter : AnouncementPresenter = AnouncementPresenter()
    var anouncementData : [Anouncement] = [Anouncement]()
    var activityIndicator : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title  = "Pengumuman"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Register Cell
        tableView.register(UINib(nibName: "AnnouncmentCell", bundle: nil), forCellReuseIdentifier: "AnnouncmentCell")
        tableView.estimatedRowHeight    = 100.5
        tableView.rowHeight             = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        self.presenter.attachView(view: self)
        self.presenter.loadData()
        
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))

        activityIndicator?.color        = UIColor.orange
        activityIndicator?.activityIndicatorViewStyle = .gray
        let barButton = UIBarButtonItem(customView: activityIndicator!)
        self.navigationItem.setRightBarButton(barButton, animated: true)
        activityIndicator?.startAnimating()
        activityIndicator?.hidesWhenStopped = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return anouncementData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncmentCell", for: indexPath) as! AnnouncmentCell
        let data = anouncementData[indexPath.row]
        cell.labelTitle.text = data.title
        cell.labelContent.text = data.message
        cell.labelDate.text = data.dateString + ", " + data.timeString
        return cell
    }
    
}

extension AnouncementVC : AnouncementView {
    func setData(value: [Anouncement]) {
        self.anouncementData = value
        self.tableView.reloadData()
    }
    
    func startLoading(message: String) {
        self.activityIndicator?.startAnimating()
    }
    
    func finishLoading(message: String) {
        self.activityIndicator?.stopAnimating()
    }
    
    func setEmptyData(message: String) {
        //
    }
    
    
}
