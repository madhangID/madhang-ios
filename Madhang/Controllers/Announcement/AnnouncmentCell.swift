//
//  AnnouncmentCell.swift
//  Madhang
//
//  Created by asharijuang on 3/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class AnnouncmentCell: UITableViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
