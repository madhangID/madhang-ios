//
//  ProfileVC.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import UnderKeyboard
import ALCameraViewController
import AlamofireImage

class ProfileVC: UIViewController {

    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    let underKeyboardLayoutConstraint = UnderKeyboardLayoutConstraint()
    // Outlet
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var viewAggreementTop: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewAgreement: UIView!
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldEmail: UITextField!
    @IBOutlet weak var fieldAddress: UITextField!
    @IBOutlet weak var fieldCity: UITextField!
    @IBOutlet weak var fieldProvince: UITextField!
    @IBOutlet weak var fieldPhone: UITextField!
    
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonAgree: UIButton!
    
    @IBOutlet weak var labelTerm: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelProvince: UILabel!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    var isUpdate : Bool = false
    
    private let presenter : ProfilePresenter = ProfilePresenter()
    var data : ProfileViewData = ProfileViewData()
    var onSuccess : (Bool) -> Void = {_ in }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.isUpdate = false
        self.setupUI()
        

        underKeyboardLayoutConstraint.setup(bottomLayoutConstraint, view: view)
        self.presenter.attachView(view: self)
        self.presenter.loadData()
        self.tabBarController?.tabBar.isHidden  = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden  = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.tabBarController?.tabBar.isHidden  = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        if isUpdate {
            self.viewAgreement.isHidden = true
            self.viewAggreementTop.constant -= (self.viewAgreement.frame.size.height + 10)
            self.containerHeight.constant -= (self.viewAgreement.frame.size.height + 10)
            // add save button navbar
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Simpan", style: .plain, target: self, action: #selector(save))
            self.title = "Profil"
        }else {
            self.viewAgreement.isHidden = false
            self.title = "Lengkapi Profil"
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        self.view.addGestureRecognizer(tapGesture)
        
        //MARK: TODO check agreement
        self.buttonSave.addTarget(self, action: #selector(register), for: .touchUpInside)
    }

    @objc func save() {
        self.data.name      = self.fieldName.text!
        self.data.email     = self.fieldEmail.text!
        self.data.phone     = self.fieldPhone.text!
        self.data.city      = self.fieldCity.text!
        self.data.provinsi  = self.fieldProvince.text!
        self.data.address   = self.fieldAddress.text!
        self.presenter.update(data: data)
    }
    
    @objc func register() {
        self.data.name      = self.fieldName.text!
        self.data.email     = self.fieldEmail.text!
        self.data.phone     = self.fieldPhone.text!
        self.data.city      = self.fieldCity.text!
        self.data.provinsi  = self.fieldProvince.text!
        self.data.address   = self.fieldAddress.text!
        self.presenter.register(data: data)
    }
    
    @objc func dismissKeyboard(sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
}

extension ProfileVC : ProfileView {
    func finishLoading(message: String) {
        //
        Helper.dissmissLoading()
    }
    func startLoading(message: String) {
        //
        Helper.showLoading()
    }
    func setEmptyData(message: String) {
        //
    }
    func setData(value: ProfileViewData, errorMessage: String) {
        self.data   = value
        self.fieldName.text     = value.name
        self.fieldEmail.text    = value.email
        self.fieldPhone.text    = value.phone
        self.fieldCity.text     = value.city
        self.fieldAddress.text  = value.address
        self.fieldProvince.text = value.provinsi
        
        if !value.phone.isEmpty {
            self.fieldPhone.isEnabled   = false
            self.fieldPhone.textColor   = UIColor.lightGray
        }
        if !errorMessage.isEmpty {
            let alert = UIAlertController(title: "Info", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func finishUpdateData(value: ProfileViewData) {
        let alert = UIAlertController(title: "Info", message: "Berhasil Update Profile", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: {
            self.onSuccess(true)
        })
    }
    func failedUpdateData(message: String) {
        print(message)
        let alert = UIAlertController(title: "Failed", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            self.onSuccess(false)
        }))
        self.present(alert, animated: true, completion: {
            //
        })
    }
    
    func progressUpdateKtp(value: Double) {
        
    }
    
    func finishUpdateKtp(url: String) {
       
    }
    
    func failedUpdatektp(message: String) {
        
    }
}
