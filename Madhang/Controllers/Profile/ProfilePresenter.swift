//
//  ProfilePresenter.swift
//  Madhang
//
//  Created by asharijuang on 1/13/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol ProfileView : BaseView {
    func setData(value: ProfileViewData, errorMessage: String)
    func finishUpdateData(value: ProfileViewData)
    func failedUpdateData(message: String)
    func progressUpdateKtp(value: Double)
    func finishUpdateKtp(url: String)
    func failedUpdatektp(message: String)
}

class ProfilePresenter {
    private let service         : ProfileService
    private var viewPresenter   : ProfileView?
    
    init() {
        self.service = ProfileService()
    }
    
    func attachView(view : ProfileView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.startLoading(message: "")
        let profile = MainApp.shared.getProfile()
        let result  = ProfileViewData()
        result.name = profile.name
        result.email    = profile.email
        result.phone    = profile.phone
        result.address  = profile.address
        result.city     = profile.city
        result.provinsi = profile.provinsi
        result.imageURL = profile.imageURL
        result.identityURL  = profile.identityURL
        result.verified = profile.verified
        
        self.viewPresenter?.setData(value: result, errorMessage: "")
        self.viewPresenter?.finishLoading(message: "")
    }
    
    func update(data: ProfileViewData) {
        self.viewPresenter?.startLoading(message: "")
        let (valid,message) = self.validate(data: data)
        if valid {
            data.id = Profile().id
            self.service.update(data: data, completion: { (response) in
                switch(response){
                case .done(value: let result):
                    // save data in db
                    let data = Profile()
                    data.name       = result.name
                    data.email      = result.email
                    data.city       = result.city
                    data.provinsi   = result.provinsi
                    data.address    = result.address
                    data.identityURL    = result.identityURL
                    self.viewPresenter?.finishUpdateData(value: result)
                    break
                case .failed(message: let message):
                    print("error message \(message)")
                    self.viewPresenter?.setData(value: data, errorMessage: message)
                default:
                    break
                }
                self.viewPresenter?.finishLoading(message: "")
            })
        }else {
            self.viewPresenter?.finishLoading(message: "")
            self.viewPresenter?.setData(value: data, errorMessage: message)
        }
    }
    
    func register(data: ProfileViewData) {
        self.viewPresenter?.startLoading(message: "")
        let (valid,message) = self.validate(data: data)
        if valid {
            data.id = Profile().id
            self.service.register(data: data, completion: { (response) in
                switch(response){
                case .done(value: let result):
                    // save data in db
                    let data = Profile()
                    data.name       = result.name
                    data.email      = result.email
                    data.city       = result.city
                    data.provinsi   = result.provinsi
                    data.address    = result.address
                    self.viewPresenter?.finishUpdateData(value: result)
                    break
                case .failed(message: let message):
                    print("error message \(message)")
                    self.viewPresenter?.failedUpdateData(message: message)
                default:
                    break
                }
                self.viewPresenter?.finishLoading(message: "")
            })
        }else {
            self.viewPresenter?.finishLoading(message: "")
            self.viewPresenter?.setData(value: data, errorMessage: message)
        }
    }
    
    func validate(data: ProfileViewData) -> (Bool,String) {
        if data.name.isEmpty {
            return (false,"Nama tidak boleh kosong")
        }
        if data.phone.isEmpty {
            return (false,"Nomor Telepon harus diisi")
        }
        if data.email.isEmpty {
            return (false,"Email tidak boleh kosong")
        }
        if data.provinsi.isEmpty {
            return (false,"Pilih salah satu provinsi")
        }
        if data.city.isEmpty {
            return (false,"Kota harus di pilih")
        }
        if data.address.isEmpty {
            return (false,"Alamat tidak boleh kosong")
        }
        return (true,"")
    }
    
}
