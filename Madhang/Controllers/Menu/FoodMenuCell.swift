//
//  FoodMenuCell.swift
//  Madhang
//
//  Created by asharijuang on 12/24/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class FoodMenuCell: UITableViewCell {

    @IBOutlet weak var imagePreorder: UIImageView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageFood: UIImageView!
    @IBOutlet weak var buttonCart: ButtonCart!
    var orderCount : Int = 0
    var foodData    : FoodViewData?
    var targetVC : UIViewController? = nil
    var updateOrder : () -> Void = { }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI(target: UIViewController) {
        self.buttonCart.delegate    = self
        self.buttonCart.text    = orderCount
        self.buttonCart.data    = foodData
        self.targetVC = target
        if foodData?.status.lowercased() == "preorder" {
            self.imagePreorder.isHidden = false
        }else {
            self.imagePreorder.isHidden = true
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(WarungHeaderCell.imageTapped(_:)))
        tap.delegate = self
        self.imageFood.addGestureRecognizer(tap)
        self.imageFood.isUserInteractionEnabled = true
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.targetVC?.view.addSubview(newImageView)
        self.targetVC?.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.targetVC?.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
    
    
}

extension FoodMenuCell: ButtonCartDelegate {
    func failedChange(message: String) {
        if let vc = targetVC {
            let alert = UIAlertController(title: "Info", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.buttonCart.replaceOrder()
                self.updateOrder()
            }))
            vc.present(alert, animated: true, completion: nil)
        }
    }
    
    func changeValue(text: Int) {
        print("now food count \(text)")
    }
    
    
}
