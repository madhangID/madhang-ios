//
//  FoodMenuVC.swift
//  Madhang
//
//  Created by asharijuang on 12/24/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

class FoodMenuVC: UIViewController {

    @IBOutlet weak var floatingCartButtom: NSLayoutConstraint!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var floatingCart: FloatingCart!
    @IBOutlet weak var tableview: UITableView!
    
    private let presenter : FoodMenuPresenter = FoodMenuPresenter()
    var foodData : [FoodViewData] = [FoodViewData]()
    var foodGroup : [String : [FoodViewData]] = [String : [FoodViewData]]()
    var shopData    : ShopViewData? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Menu"
        // Register Cell
        self.tableview.register(UINib(nibName: "FoodMenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        
        // remove empty cell
        tableview.tableFooterView = UIView()
        
        self.presenter.attachView(view: self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        floatingCart.setupUI(Controller: self)
        self.floatingCart.delegate  = self
        setupFloatingCart()
        self.presenter.loadData(shop: self.shopData)
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            // Your code...
            self.presenter.clear()
        }
    }

    
    func setupFloatingCart() {
        if !OrderManager.shared.isCartActive() {
            self.floatingCartButtom.constant    = self.floatingCart.frame.height
            self.floatingCart.isHidden          = true
        }else {
            self.floatingCart.isHidden          = false
            self.floatingCartButtom.constant    = 0
        }
    }
}

extension FoodMenuVC: UITableViewDelegate {
    
}

extension FoodMenuVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.foodGroup.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = presenter.menuCategory(menu: self.foodGroup)
        if category.count > 0 {
            return self.foodGroup[category[section]]!.count
        }else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! FoodMenuCell
        let category = presenter.menuCategory(menu: self.foodGroup)
        if category.count > 0 {
            let key = category[indexPath.section]
            let foodData = self.foodGroup[key]![indexPath.row]
            cell.labelName.text     = foodData.name
            cell.labelPrice.text    = String(foodData.price)
            cell.labelDescription.text  = foodData.desc
            cell.imageFood.af_setImage(withURL: URL(string: foodData.imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
            cell.foodData           = foodData
            cell.orderCount         = foodData.total
            cell.setupUI(target: self)
            cell.updateOrder = {
            self.presenter.loadData(shop: self.shopData)
//                buggy , somehow button not match with count
//                self.presenter.syncOrder()
//                self.tableview.reloadData()
            }
        }
        
        return cell
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        let category = presenter.menuCategory(menu: self.foodGroup)
        return category[section].capitalizingFirstLetter()
    }
    
    func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        return 20.0
    }
}

extension FoodMenuVC : FloatingCartDelegate {
    func didUpdate() {
        //
        self.setupFloatingCart()
    }
}

extension FoodMenuVC : FoodMenuView {
    func setDataGroup(value: [String : [FoodViewData]]) {
        self.foodGroup = value
        self.tableview.reloadData()
    }
    
    func setData(value: [FoodViewData]) {
        self.foodData   = value
        self.tableview.reloadData()
    }
    
    func startLoading(message: String) {
        //
        self.tableview.isHidden = true
        self.labelInfo.text     = "Mohon Tunggu"
        self.loadingIndicator.startAnimating()
    }
    
    func finishLoading(message: String) {
        //
        self.tableview.isHidden = false
        self.labelInfo.text     = ""
        self.loadingIndicator.stopAnimating()
    }
    
    func setEmptyData(message: String) {
        //
        self.tableview.isHidden = true
        self.labelInfo.text     = "Maaf, Kami sedang menyiapkan resep makanan terbaik. Silahkan berkunjung di lain waktu"
    
    }
    
    
}
