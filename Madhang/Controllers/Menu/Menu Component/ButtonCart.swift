//
//  ButtonCart.swift
//  Madhang
//
//  Created by asharijuang on 12/24/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

protocol ButtonCartDelegate {
    func changeValue(text: Int)
    func failedChange(message: String)
}

@IBDesignable class ButtonCart: UIView {
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var buttonMin: UIButton!
    var data        : FoodViewData? = nil
    var delegate : ButtonCartDelegate?
    
    var text: Int = 0 {
        didSet {
            if label != nil {
                self.setupUI()
            }
        }
    }

    // If someone is to initialize a WeatherView in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // If someone is to initalize a WeatherView in Storyboard setting the Custom Class of a UIView
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // 1. Load the nib named 'WeatherView' into memory, finding it in the main bundle.
        Bundle.main.loadNibNamed("ButtonCart", owner: self, options: [:])
        
        // 2. Adding the 'contentView' to self (self represents the instance of a WeatherView which is a 'UIView').
        addSubview(contentView)
        
        // 3. Setting this false allows us to set our constraints on the contentView programtically
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        // 4. Setting the constraints programatically
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    func setupUI() {
        if text == 0 {
            self.editable(value: false)
        }else {
            self.editable(value: true)
        }
        self.label.text = String(describing: text)
        self.delegate?.changeValue(text: text)
    }
    
    func editable(value: Bool) {
        if value == true {
            self.buttonAdd.isHidden = true
            self.buttonMin.isHidden = false
        }else {
            self.buttonAdd.isHidden = false
            self.buttonMin.isHidden = true
        }
    }
    
    func replaceOrder() {
        OrderManager.shared.replace(data: data!) { (message) in
            self.delegate?.failedChange(message: message)
        }
    }
    
    @IBAction func clickBuy(_ sender: Any) {
        self.text = self.text + 1
        if data != nil {
            OrderManager.shared.add(data: data!, onError: { (message) in
                self.delegate?.failedChange(message: message)
            })
        }
    }
    
    @IBAction func clickAdd(_ sender: Any) {
        self.text = self.text + 1
        if data != nil {
            OrderManager.shared.add(data: data!, onError: { (message) in
                self.delegate?.failedChange(message: message)
            })
        }
    }
    
    @IBAction func clickMin(_ sender: Any) {
        self.text = self.text - 1
        if data != nil {
            OrderManager.shared.min(data: data!, onError: { (message) in
                self.delegate?.failedChange(message: message)
            })
        }
    }
    
    
}
