//
//  FoodCell.swift
//  Madhang
//
//  Created by qiscus on 26/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var labelOrderDate: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelOrderStatus: UILabel!
    @IBOutlet weak var labelIdOrder: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
