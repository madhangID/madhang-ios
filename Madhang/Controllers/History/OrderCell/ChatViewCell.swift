//
//  ChatViewCell.swift
//  Madhang
//
//  Created by asharijuang on 1/27/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class ChatViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonChat: UIButton!
    var tapChat : () -> Void = { }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickChat(_ sender: Any) {
        tapChat()
    }
}
