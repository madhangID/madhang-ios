//
//  OrderTransactionCell.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class OrderTransactionCell: UITableViewCell {

    @IBOutlet weak var labelOrderType: UILabel!
    @IBOutlet weak var labelShopAddress: UILabel!
    @IBOutlet weak var labelShopName: UILabel!
    @IBOutlet weak var labelOrderNumber: UILabel!
    @IBOutlet weak var labelInfoOrderNumber: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelOrderStatus: UILabel!
    @IBOutlet weak var labelOrderTime: UILabel!
    
    var orderTime : String {
        set {
            self.labelOrderTime.text = "Pesanan dilayani pada \(newValue)"
        }
        get {
            return self.orderTime
        }
    }
    
    var orderStatus : OrderHistoryStatus {
        set {
            self.labelOrderStatus.text  = newValue.rawValue
        }
        get {
            return self.orderStatus
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
