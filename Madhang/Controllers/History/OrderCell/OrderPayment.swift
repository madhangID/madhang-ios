//
//  OrderPayment.swift
//  Madhang
//
//  Created by asharijuang on 3/31/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

class OrderPayment: UITableViewCell {

    @IBOutlet weak var labelSubTotal: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelPaymentMethod: UILabel!
    @IBOutlet weak var labelVoucher: UILabel!
    
    var voucher: String {
        set {
            self.labelVoucher.text    = "Voucher [\(newValue)]"
        }
        get {
            return self.voucher
        }
    }
    var subTotal: Int {
        set {
            self.labelSubTotal.text    = "Rp. \(newValue),-"
        }
        get {
            return self.subTotal
        }
    }
    var discount: Int {
        set {
            self.labelDiscount.text    = "Rp. \(newValue),-"
        }
        get {
            return self.discount
        }
    }
    var total: Int {
        set {
            self.labelTotal.text    = "Rp. \(newValue),-"
        }
        get {
            return self.total
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
