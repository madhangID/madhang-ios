//
//  HistoryVC.swift
//  Madhang
//
//  Created by asharijuang on 16/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

class HistoryVC: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelInformation: UILabel!
    @IBOutlet weak var switchHistory: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var selectedHistory : Int   = 0
    let presenter        : OrderPresenter     = OrderPresenter()
    var orderInProgress  : [OrderHistory]    = [OrderHistory]()
    var orderFinish      : [OrderHistory]    = [OrderHistory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "History"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // remove empty cell
        tableView.tableFooterView = UIView()
        // Register Cell
        self.tableView.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeHistoryTyp(_ sender: UISegmentedControl) {
        switch switchHistory.selectedSegmentIndex {
        case 0:
            self.selectedHistory    = 0
            break
        case 1:
            self.selectedHistory    = 1
            break
        default:
            break
        }
        self.getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden   = true
        self.tabBarController?.tabBar.isHidden  = false
        self.presenter.attachView(view: self)
        self.switchHistory.selectedSegmentIndex = self.selectedHistory
        self.getData()
    }
    
    @IBAction func getData(_ sender: Any? = nil) {
        self.tableView.reloadData()
        if selectedHistory == 0 {
            self.presenter.loadData(status: OrderStatus.onProgress)
        }else {
            self.presenter.loadData(status: OrderStatus.done)
        }
    }
    
    func getDataActive() -> [OrderHistory] {
        if selectedHistory == 0 {
            return self.orderInProgress
        }else {
            return self.orderFinish
        }
    }
}

extension HistoryVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension HistoryVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        let data = self.getDataActive()[indexPath.row]
        cell.labelName.text = data.shop.name
        cell.labelIdOrder.text = data.order.id
        cell.labelOrderStatus.text  = data.status.rawValue
        cell.labelTotal.text        = "Rp. \(data.total),-"
        cell.labelOrderDate.text    = "\(data.order.orderDate) \(data.order.orderTime)"
        cell.imageAvatar.af_setImage(withURL: URL(string: data.shop.imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getDataActive().count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.getDataActive()[indexPath.row]
        let target = OrderDetailVC()
        target.data = data
        self.navigationController?.pushViewController(target, animated: true)
    }
}

extension HistoryVC : OrderHistoryView {
    func setOrder(status: OrderStatus, value: [OrderHistory]) {
        if status == .done {
            self.orderFinish = value
        }else {
            self.orderInProgress = value
        }
        self.tableView.reloadData()
    }
    
    func willUpdate() {
        //
    }
    
    func startLoading(message: String) {
        self.tableView.isHidden = true
        self.loadingIndicator.startAnimating()
        self.labelInformation.text  = message
    }
    
    func finishLoading(message: String) {
        //
        self.tableView.isHidden = false
        self.loadingIndicator.stopAnimating()
    }
    
    func setEmptyData(message: String) {
        //
        self.loadingIndicator.stopAnimating()
    }
    
    
}
