//
//  OrderDetailVC.swift
//  Madhang
//
//  Created by asharijuang on 12/25/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AlamofireImage

class OrderDetailVC: UIViewController {
    
    @IBOutlet weak var constraintComunicationButton: NSLayoutConstraint!
    @IBOutlet weak var constraintFooterHeight: NSLayoutConstraint!
    @IBOutlet weak var viewFooterArea: UIView!
    @IBOutlet weak var viewBasicButton: UIView!
    @IBOutlet weak var tableView : UITableView!
    var data : OrderHistory? = nil
    let presenter        : OrderDetailPresenter     = OrderDetailPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Detil Order"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        // Register Cell
        self.tableView.register(UINib(nibName: "OrderTransactionCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        self.tableView.register(UINib(nibName: "OrderItemCell", bundle: nil), forCellReuseIdentifier: "ItemCell")
        self.tableView.register(UINib(nibName: "OrderNoteCell", bundle: nil), forCellReuseIdentifier: "OrderNoteCell")
        self.tableView.register(UINib(nibName: "ChatViewCell", bundle: nil), forCellReuseIdentifier: "OrderChatCell")
        self.tableView.register(UINib(nibName: "OrderOptionCell", bundle: nil), forCellReuseIdentifier: "OrderOptionCell")
        self.tableView.register(UINib(nibName: "OrderPayment", bundle: nil), forCellReuseIdentifier: "OrderPaymentCell")
        
        self.navigationController?.navigationBar.isHidden   = false
        self.tabBarController?.tabBar.isHidden  = true
        
        // remove empty cell
        tableView.tableFooterView = UIView()
        self.presenter.attachView(view: self)
        
        // Setup footer
        if self.data?.status != OrderHistoryStatus.pending {
            self.constraintComunicationButton.constant = 0.33 * self.viewFooterArea.bounds.width
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func clickCall(_ sender: Any) {
        if !(self.data?.shop.phone.isEmpty)! {
            if let phone = self.data?.shop.phone {
                guard let number = URL(string: "tel://" + phone) else { return }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(number)
                } else {
                    UIApplication.shared.openURL(number)
                }
            }else {
                // phone not found
                Helper.showError(message: "Phone number tidak tersedia")
            }
        }else {
            // phone not found
            Helper.showError(message: "Phone number tidak tersedia")
        }
    }
    @IBAction func clickChat(_ sender: Any) {
        self.presenter.chat(id: (self.data?.shop.id)!, target: self)
    }
    @IBAction func clickCancel(_ sender: Any) {
        self.presenter.cancelOrder(id: (self.data?.order.id)!)
    }
}

extension OrderDetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section    == 0 {
            return 260 // detail transaction
        }else if indexPath.section    == 1 {
            return 200 // Note ant detail header
        }else if indexPath.section    == 2 {
            return 60 // items menu
        }else if indexPath.section    == 3 {
            return 155 // detail payment
        }else if indexPath.section    == 4 {
            return 120
        }else if indexPath.section    == 5 {
            return 150
        }
        return 0.0
    }
}

extension OrderDetailVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            // Detail Transaksi
            return 1
        }else if section == 1 {
            // Order Note
            return 1
        }else if section == 2 {
            // Order Item
            return self.data?.order.items!.count ?? 0
        }else if section == 3 {
            // total
            return 1
        /** new ui feedback bayu ceo
        }else if section == 4 {
            // Order chat
            if self.data?.status == OrderHistoryStatus.rejected || self.data?.status == OrderHistoryStatus.finished {
                return 0
            }else {
                return 1
            }
        }else if section == 5 {
            // Order option
            if self.data?.status == OrderHistoryStatus.pending {
                return 1
            }else {
                return 0
            }
        */
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderTransactionCell
            cell.orderStatus  = (data?.status)!
            cell.labelOrderNumber.text  = data?.order.id
            cell.orderTime  = (data?.order.orderDate)!
            cell.labelShopName.text = data?.shop.name
            cell.labelShopAddress.text = data?.shop.address
            cell.labelOrderType.text = data?.order.delivery.rawValue
            if let imageUrl = data?.shop.imageUrl {
                cell.imageAvatar.af_setImage(withURL: URL(string: imageUrl)!, placeholderImage: UIImage(named: "logo"), completion: nil)
            }
            return cell
        }else if indexPath.section  == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderNoteCell", for: indexPath) as! OrderNoteCell
            cell.labelNote.text = data?.order.note
            return cell
        }else if indexPath.section  == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! OrderItemCell
            if let item = self.data?.order.items![indexPath.row] {
                cell.labelCount.text    = String(item.total)
                cell.labelName.text     = item.name
                cell.labelPrice.text    = "Rp. \(item.price),-"
            }
            return cell
        }else if indexPath.section  == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderPaymentCell", for: indexPath) as! OrderPayment
            if let data = self.data {
                cell.subTotal = data.subTotal
                cell.total    = data.total
                cell.discount = data.discount
                cell.voucher = data.voucher
                cell.labelPaymentMethod.text = data.order.payment.rawValue
            }
            return cell
        }else {
            return UITableViewCell()
        }
    }
}

extension OrderDetailVC : OrderDetailView {
    func successCancelOrder(message: String) {
        Helper.showFlash(message: message)
        self.navigationController?.popViewController(animated: true)
    }
    
    func startLoading(message: String) {
        Helper.showLoading(message: message)
    }
    
    func finishLoading(message: String) {
        Helper.dissmissLoading()
    }
    
    func setEmptyData(message: String) {
        //
    }
}
