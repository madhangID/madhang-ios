//
//  HomeVC.swift
//  Madhang
//
//  Created by asharijuang on 16/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var textFieldSearch: UISearchBar!
    var categoryVC : FoodCategoryVC = FoodCategoryVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.title  = "Explore"
        let backItem    = UIBarButtonItem()
        backItem.title  = ""
        self.navigationItem.backBarButtonItem = backItem
        self.setupCategory()
        
        let barButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.done, target: self.textFieldSearch.inputView, action: #selector(dismissKeyboard))
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar.items = [barButton]

        self.textFieldSearch.inputAccessoryView = toolbar
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden  = false
        self.navigationController?.isNavigationBarHidden = true
        MainApp.shared.getCurrentLocation { (lat, long) in
            if lat == nil || long == nil {
                let alert = UIAlertController(title: "Information", message: "Maaf, kami tidak dapat mengakses lokasi anda. Aktifkan GPS Location pada Device Anda.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Setting", style: UIAlertActionStyle.default, handler: { (action) in
                    MainApp.shared.locationSetting()
                }))
//                alert.addAction(UIAlertAction(title: "Pilih Kota", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.dismissKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCategory() {
        //
        categoryVC.view.frame.size  = viewCategory.frame.size
        self.addChildViewController(categoryVC)
        self.viewCategory.addSubview(categoryVC.view)
        categoryVC.didMove(toParentViewController: self)
    }

    func dismissKeyboard(sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
}

extension HomeVC : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !(self.textFieldSearch.text?.isEmpty)! {
            let target = SearchFoodVC()
            target.keywords = self.textFieldSearch.text!
            self.navigationController?.pushViewController(target, animated: true)
        }
        self.textFieldSearch.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.textFieldSearch.text = ""
        self.textFieldSearch.resignFirstResponder()
    }
    
}
