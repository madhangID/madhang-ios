//
//  ChatManager.swift
//  Madhang
//
//  Created by asharijuang on 1/27/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import Qiscus

class ChatManager {
    
    private var appID   : String = Helper.CHAT_APP_ID
    private var userKey : String = "edbaf3a0c6fe7907d29f34cbe88c0457" // userkey hardcode
    var sourceTargetChat : UIViewController? = nil
    
    init() {
        //
    }
    
    func setup(userID: String, username: String, avatarUrl: String) {
        Qiscus.setup(withAppId: self.appID, userEmail: userID, userKey: self.userKey, username: username, avatarURL: avatarUrl, delegate: self, secureURl: true)
        Qiscus.showDebugPrint = true
    }
    
    func update(username: String, avatar: String) {
        Qiscus.updateProfile(username: username, avatarURL: avatar, onSuccess: {
            print("qiscus success update profile")
        }) { (error) in
            print("error update profile")
        }
    }
    
    func chatWith(withUser user: String, target: UIViewController) {
        self.sourceTargetChat   = target
        let chatview = Qiscus.chatView(withUsers: [user])
        target.navigationController?.pushViewController(chatview, animated: true)
    }
}

extension ChatManager : QiscusConfigDelegate {
    func qiscusConnected() {
        print("qiscus connect")
    }
    
    func qiscusFailToConnect(_ withMessage: String) {
        print("Qiscus failed connect \(withMessage)")
        if let target = self.sourceTargetChat {
            target.navigationController?.popToRootViewController(animated: true)
        }
        Helper.showError(message: "Mohon Maaf, percakapan tidak aktif")
    }
}
