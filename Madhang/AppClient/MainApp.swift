//
//  MainApp.swift
//  Madhang
//
//  Created by asharijuang on 16/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

protocol mainAppDelegate{
    func appWithValidUser()
    func appWithInvalidUser()
}

class MainApp {
    public static let shared = MainApp()
    private var appDelegate : AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    var delegate        : mainAppDelegate?
    private var authService     : ClientAuth!
    private var chatService     : ChatManager!
    private var analytics       : ClientAnalytic!
    private var profileManager  : ProfileManager!
    private var locationManager : LocationManager!
    private var utilities       : Utilities!
    private init() {
    }
    
    func setup(withDelegate del: mainAppDelegate? = nil){
        self.delegate    = del
        authService     = ClientAuth()
        chatService     = ChatManager()
        analytics       = ClientAnalytic()
        profileManager  = ProfileManager()
        locationManager = LocationManager()
        utilities       = Utilities()
        self.validateUser()
        self.locationManager.update()
    }
    
    func logout() {
        // check demo mode
        let profile = Profile()
        if profile.demo && profile.phone == Helper.DEMO_NUMBER {
            self.setup()
        }else {
            // clear OTP auth
            self.authService.logout()
        }
        self.profileManager.clear()
    }
    
    func demoUser(phone: String) {
        // bypass OTP
        self.authService.demoLogin(phone: phone)
        self.checkProfile()
    }
    
    func validateUser() {
        if self.authService.isLogined() {
            self.checkProfile()
        }else {
            appDelegate.window?.rootViewController = LoginVC()
        }
        self.updateFacility()
    }
    
    func checkProfile() {
        // profile already exist or load from backend
        if self.profileManager.isComplete() {
            self.setupUI()
            let profile = Profile()
            self.chatService.setup(userID: profile.id, username: profile.name, avatarUrl: profile.imageURL)
        }else {
            appDelegate.window?.rootViewController = MainVC()
            // check and populate data profile
            self.profileManager.checkProfile(completion: { (isCompleteProfile) in
                if !isCompleteProfile {
                    let target = self.profileManager.completeProfileVC(onSuccess: { (success) in
                        self.validateUser()
                    })
                    self.appDelegate.window?.rootViewController = target
                }else {
                    self.setupUI()
                    let profile = Profile()
                    self.chatService.setup(userID: profile.id, username: profile.name, avatarUrl: profile.imageURL)
                }
            })
        }
    }
    
    func getPhone(accessToken: String, completion : @escaping (String) -> Void) {
        self.authService.getPhone(accessToken: accessToken, completion: completion)
    }
    
    func loginViewController() -> UIViewController {
        return self.authService.loginViewController()
    }
    
    // Chat Wrapper
    func chat(withUser user: String, target: UIViewController) {
        self.chatService.chatWith(withUser: user, target: target)
    }
    
    // MARK : Navigation UI
    func setupUI() {
        // Set up the first View Controller
        let nav1 = self.defaultNavigation()
        let vc1 = HomeVC()
        vc1.tabBarItem.title = "Explore"
        vc1.tabBarItem.image = UIImage(named: "find")
        nav1.setViewControllers([vc1], animated: true)
        
        // Set up chats controller
        let nav2 = self.defaultNavigation()
        let vc2 = HistoryVC()
        vc2.tabBarItem.title = "History"
        vc2.tabBarItem.image = UIImage(named: "list")
        nav2.setViewControllers([vc2], animated: true)
        
        //Set up merchant account controller
        let nav3 = self.defaultNavigation()
        let vc3 = RoomListVC()
        vc3.tabBarItem.title = "Chat"
        vc3.tabBarItem.image = UIImage(named: "chat")
        nav3.setViewControllers([vc3], animated: true)
        
        // Set up the settings View Controller
        let nav5 = self.defaultNavigation()
        let vc5 = SettingVC()
        vc5.tabBarItem.title = "Profile"
        vc5.tabBarItem.image = UIImage(named: "profile")
        nav5.setViewControllers([vc5], animated: true)
        
        let tabBarController = UITabBarController()
        // remove (callVc)
        tabBarController.viewControllers = [nav1, nav2, nav3, nav5]
        
        // attribute tab bar
        tabBarController.selectedIndex = 0
        tabBarController.tabBar.tintColor = Style.navigationBarTitleColor
        // Make the Tab Bar Controller the root view controller
        appDelegate.window?.rootViewController = tabBarController
        self.track(event: "Home Page")
    }
    
    private func defaultNavigation() -> UINavigationController {
        let navController = UINavigationController()
        navController.navigationBar.barTintColor = Style.navigationBarColor
        navController.navigationBar.tintColor = Style.navigationBarTitleColor
        navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : Style.navigationBarTitleColor]
        return navController
    }
    
    // Profile
    func getProfile() -> ProfileViewData {
        // update profile
        self.profileManager.checkProfile(completion: { (isCompleteProfile) in
            // profile updated
        })
        return self.profileManager.getProfile()
    }

    func getProfileVC() -> UIViewController {
        return UIViewController()
    }

    // Location
    func getLocation() -> (Double, Double) {
        let result = self.locationManager.location
        if let loc = result {
            return (loc.coordinate.latitude,loc.coordinate.longitude)
        }else {
            // Default loc
            return (-6.983144,110.4129621)
        }
    }
    
    func getCurrentLocation(location: @escaping (_ lat: Double?, _ long: Double?) -> Void) {
        self.locationManager.request { (result) in
            if let loc = result {
                location(loc.coordinate.latitude,loc.coordinate.longitude)
            }else {
                // Default loc
                location(nil,nil)
            }
        }
    }
    
    func locationSetting() {
        self.locationManager.locationSetting()
    }
    
    // MARK: Utilities
    func updateFacility() {
        self.utilities.updateFacilities()
    }
    
    func getFacility(id: String) -> Facility? {
        return self.utilities.getFacility(id: id)
    }
    
    func getFacilities() -> [Facility] {
        return self.utilities.facilities
    }
    
    // MARK: Analitycs
    func track(event: String, Property: [String:Any]? = nil) {
        self.analytics.track(event: event, Property: Property)
    }
}
