//
//  ProfileManager.swift
//  Madhang
//
//  Created by asharijuang on 1/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import UIKit

class ProfileManager {
    var data : Profile {
        get { return Profile() }
    }
    let service         = ProfileService()
    func isComplete() -> Bool  {
        if data.name.isEmpty || data.phone.isEmpty {
            return false
        }else {
            return true
        }
    }
    
    // check on backend profile is new or old
    func checkProfile(completion : @escaping (Bool) -> Void) {
        let profile = self.getProfile()
        service.checkProfile(phone: profile.phone) { (response) in
            switch response {
            case .done((let isNew)):
                completion(isNew)
                break
            case .failed(message: let message):
                print("error check login \(message)")
                completion(false)
                break
            default: break
            }
        }
    }
    
    func completeProfileVC(onSuccess : @escaping (Bool) -> Void) -> UIViewController {
        let target = ProfileVC()
        target.onSuccess = onSuccess
        return target
    }
    
    func getProfile() -> ProfileViewData {
        // convert view model
        let result = ProfileViewData()
        let profile = self.data
        result.name     = profile.name
        result.phone    = profile.phone
        result.email    = profile.email
        result.provinsi = profile.provinsi
        result.address  = profile.address
        result.city     = profile.city
        result.coin     = profile.coin
        result.credits  = profile.credits
        result.gender   = profile.gender
        result.identityURL  = profile.identityURL
        result.imageURL     = profile.imageURL
        result.verified = profile.verified
        return result
    }
    
    func updateProfile() {
        
    }
    
    func clear() {
        Profile().clear()
    }
}
