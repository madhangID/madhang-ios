//
//  AuthHandler.swift
//  Madhang
//
//  Created by asharijuang on 1/1/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import Alamofire

class AuthHandler : RequestAdapter  {
    private var deviceId:String? = nil
    private var appId:String? = Helper.API_KEY 
    private var accessToken     : String = ""
    private var userAgent       : String = ""
    
    
    var headers: HTTPHeaders = [
        "user-agent": "ios",
        "Content-Type":"application/json",
        ]
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue("Token token=" + accessToken, forHTTPHeaderField: "Authorization")
        urlRequest.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        
        if(appId != nil) {
            urlRequest.setValue(appId!, forHTTPHeaderField: "x-api-key")
            print("appId:\(appId!)")
        }
        
        if(deviceId != nil) {
            urlRequest.setValue(deviceId!, forHTTPHeaderField: "X-DEVICE-ID")
            print("deviceId:\(deviceId!)")
        }
        
        return urlRequest
    }
    
    
}
