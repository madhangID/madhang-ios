//
//  OrderManager.swift
//  Madhang
//
//  Created by asharijuang on 1/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

class OrderManager {
    public static let shared = OrderManager()
    private var items  : [OrderItem] = [OrderItem]()
    private var order   : Order?    = nil
    
    var totalPrice      : Int  {
        get {
            return self.getTotal(order: getOrderItem())
        }
    }
    var subTotal : Int  {
        get {
            return self.getSubTotal(order: getOrderItem())
        }
    }
    var status  : String = "" // preorder, biasa tidak bisa dicampur
    var discount      : Int  = 0
    var voucher     : String = ""
    var update : ([OrderItem]) -> Void = {_ in}
    
    init() {
        
    }
    
    func clear() {
        self.clearOrder()
    }
    
    func isCartActive() -> Bool {
        if self.getOrderItem().count > 0 {
            return true
        }else {
            return false
        }
    }
    
    private func getSubTotal(order : [OrderItem]) -> Int {
        var result  = 0
        if order.count > 0 {
            for o in order {
                if o.total > 0 {
                    result += (o.price * o.total)
                }
            }
        }
        return result
    }
    
    private func getTotal(order : [OrderItem]) -> Int {
        return getSubTotal(order: order) - self.discount
    }
    
    func sync(data: [FoodViewData]) -> [FoodViewData] {
        let dataOrders  = getOrderItem()
        var newData     = data
        for (index,i) in newData.enumerated() {
            if let order = dataOrders.first(where: {$0.id == i.id}) {
                // do something with order
                newData[index].total = order.total
            }
        }
        return newData
    }
    
    func getOrder() -> Order? {
        return self.order
    }
    
    func getNote() -> String {
        // MARK: Todo refactor this
        if !(self.order != nil) {
            self.order  = Order()
        }
        return (self.order?.note)!
    }
    
    func setNote(value: String) {
        self.order?.note = value
    }
    
    func getOrderItem() -> [OrderItem] {
        if items.count > 0 {
            return self.items
        }else {
            // if count 0 always check in storage
            let data = self.loadItems()
            if data != nil {
                self.items = data!
                return data!
            }else {
                return self.items
            }
        }
    }
    
    func replace(data: FoodViewData, onError: @escaping (String) -> Void) {
        self.clear()
        self.add(data: data, onError: onError)
    }
    
    func add(data: FoodViewData, onError: @escaping (String) -> Void) {
        // check order with other warung
        if data.shopID != self.items.first?.shopID && !self.items.isEmpty {
            onError("Anda menambahkan makanan dari Warung yang berbeda, Hapus pesanan dari warung sebelumnya?")
            return
        }
        if data.status != self.status && !self.status.isEmpty {
            onError("Anda tidak dapat menambahkan menu \(data.status) ke dalam menu \(self.status), Hapus menu pesanan sebelumnya?")
            return
        }
        self.status = data.status
        // find in array. if exist increase total : add in array
        if let order = self.items.first(where: {$0.id == data.id}) {
            // do something with order
            order.total += 1
        } else {
            // item could not be found
            let order = OrderItem(id: data.id, name: data.name, price: data.price, total: 1, type: data.status)
            order.desc          = data.desc
            order.imageUrl      = data.imageUrl
            order.category      = data.category
            order.time          = data.time
            order.shopID        = data.shopID
            order.shopOwnerID   = data.shopOwnerID
            self.items.append(order)
        }
        self.saveItems(order: self.items)
    }
    
    func min(data: FoodViewData, onError: @escaping (String) -> Void) {
        if let index = self.items.index(where: {$0.id == data.id}) {
            // do something with index
            if self.items[index].total > 1 {
                self.items[index].total -= 1
            }else {
                // total 1 and remove from order cart
                self.items.remove(at: index)
            }
            self.saveItems(order: self.items)
        } else {
            // item could not be found
        }
    }
    
    func setOrderDate(date : Date) {
        // do something after tapping done
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat    = "dd/MM/yyyy"
        // again convert your date to string
        let dateOrder           = formatter.string(from: yourDate!)
        
        if !(self.order != nil) {
            self.order  = Order()
        }
        self.order?.orderDate   = dateOrder
    }
    
    func setOrderTime(date : Date) {
        // do something after tapping done
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat    = "HH:mm"
        let timeOrder           = formatter.string(from: yourDate!)
        
        if !(self.order != nil) {
            self.order  = Order()
        }
        self.order?.orderTime   = timeOrder
    }
    
    func setOrderGuest(number: Int) {
        if !(self.order != nil) {
            self.order  = Order()
        }
        self.order?.numberGuest = number
    }
    
    func validate() -> (Bool,String) {
        if self.order == nil {
            return (false,"Mohon Periksa Kembali Order anda")
        }
        
        let message = ""
        var result = false
        // Check number items
        if self.getOrderItem().count  > 0 {
            // check number of guest and order date time
            if self.order?.numberGuest != 0 && !(self.order?.orderDate.isEmpty)! && !(self.order?.orderTime.isEmpty)! {
                result  = true
            }
        }
        return (result,message)
    }
    
    // API Helper
    func getParams(order: Order) -> [String:Any] {
        var param : [String : Any] = [
            "pemesanuserid"     : Profile().id,
            "catatan"           : self.order!.note,
            "jenis_pesanan"     : order.delivery.rawValue,
            "jumlah_tamu"       : self.order!.numberGuest,
            "type_menu"         : order.type,
            "typepembayaran"    : order.payment.rawValue,
            "tanggal"           : self.order!.orderDate,
            "time"              : self.order!.orderTime,
            "total"             : order.total,
            "totallama"         : self.subTotal,
            "kodekupon"         : self.voucher,
            ]
        
        for i in self.getOrderItem() {
            let key = "makanan[\(i.id)]"
            param[key] = i.toJSON()
            param["tenantuserid"]   = i.shopOwnerID
            param["tenantidtoko"]   = i.shopID
        }
        
        return param
        
    }
    
    // Storage
    private func saveItems(order : [OrderItem]) {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: order)
        let defaults = UserDefaults.standard
        defaults.set(archivedObject, forKey: "md_OrderItem")
        defaults.synchronize()
        // triger cart update
        self.update(order)
    }
    
    private func loadItems() -> [OrderItem]? {
        if let decodedNSDataBlob = UserDefaults.standard.object(forKey: "md_OrderItem") as? Data {
            if let savedUser = NSKeyedUnarchiver.unarchiveObject(with: decodedNSDataBlob) as? [OrderItem] {
                return savedUser
            }else {
                return nil
            }
        }else {
            return nil
        }
        
    }
    
    private func clearOrder() {
        // clear item order
        self.items.removeAll()
        self.status     = ""
        self.voucher    = ""
        self.discount   = 0
        UserDefaults.standard.removeObject(forKey: "md_OrderItem")
    }
    
}
