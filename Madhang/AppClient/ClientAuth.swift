//
//  ClientAuth.swift
//  Madhang
//
//  Created by asharijuang on 16/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import AccountKit
import Alamofire

class ClientAuth: NSObject {
    var mockLogin : Bool = false
    var accountKit: AKFAccountKit!
    let service : AuthService = AuthService()
    
    override init() {
        super.init()
        self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
        self.setAuth()
    }
    
    func setAuth() {
        // get a session manager and add the request adapter
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.adapter = AuthHandler()
    }
    
    // Demo Login
    func demoLogin(phone: String) {
        // demo account
        if phone == Helper.DEMO_NUMBER {
            let profile = Profile()
            profile.phone = phone
            profile.demo = true
        }
    }
    
    func isLogined() -> Bool {
        // Account kit and old user
        print("accesstoken \(accountKit.currentAccessToken?.tokenString)")
        if (accountKit.currentAccessToken != nil && !Profile().phone.isEmpty) {
            return true
        }else {
            return false
        }
    }
    
    func getPhone(accessToken: String, completion : @escaping (String) -> Void) {
        service.getPhone(accessToken: accessToken) { (response) in
            switch response {
            case .done(let phone):
                completion(phone)
                break
            default:
                completion("")
                break
            }
        }
    }
    
    func loginViewController() -> UIViewController {
        return UIViewController()
    }
    
    func logout() {
        self.accountKit.logOut { (success, error) in
            print("logout success \(success)")
            // Bugs logout need logout 2x
            if (self.accountKit.currentAccessToken != nil) {
                self.logout()
            }else {
                MainApp.shared.setup()
            }
        }
    }
}
