//
//  Utilities.swift
//  Madhang
//
//  Created by asharijuang on 2/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

class Utilities {
    var facilities = [Facility]()
    var service : ToolService
    init() {
        self.service    = ToolService()
    }
    
    func getFacility(id: String) -> Facility? {
        if let result = facilities.first(where: { $0.id == id }) {
            return result
        }else {
            return nil
        }
    }
    
    func updateFacilities() {
        service.facilities { (response) in
            switch response {
            case .done((let data)):
                self.facilities = data
                self.saveFacilities(data: data)
                break
            case .failed(message: let message):
                print("message \(message)")
                break
            default: break
            }
        }
    }
    
    // Storage
    private func saveFacilities(data : [Facility]) {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: data)
        let defaults = UserDefaults.standard
        defaults.set(archivedObject, forKey: "md_Facilities")
        defaults.synchronize()
    }
    
    private func loadFacilities() -> [Facility]? {
        if let decodedNSDataBlob = UserDefaults.standard.object(forKey: "md_Facilities") as? Data {
            if let savedUser = NSKeyedUnarchiver.unarchiveObject(with: decodedNSDataBlob) as? [Facility] {
                return savedUser
            }else {
                return nil
            }
        }else {
            return nil
        }
        
    }
}
