//
//  ClientAnalytics.swift
//  Madhang
//
//  Created by asharijuang on 1/6/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import Mixpanel

let ANALYTIC_KEY    = "9d30455bbbee0ad1986a06c9f07b75e9"

class ClientAnalytic {
    //
    let client = Mixpanel.sharedInstance()
    
    init() {
        Mixpanel.init(token: ANALYTIC_KEY, launchOptions: nil, flushInterval: 60, trackCrashes: true)
    }
    
    func track(event: String, Property: [String:Any]? = nil) {
        Mixpanel.sharedInstance()?.track("Video play")
        if Property == nil {
            client?.track(event)
        }else {
            client?.track(event, properties: Property)
        }
    }
}
