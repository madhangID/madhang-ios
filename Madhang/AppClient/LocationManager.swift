//
//  LocationManager.swift
//  Madhang
//
//  Created by asharijuang on 1/14/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftLocation
import CoreLocation

class LocationManager : NSObject {
    var location : CLLocation? = nil
    
    override init() {
        super.init()
    }
    
    func update() {
        self.request { (loc) in
            self.location = loc
        }
    }
    
    func request(location: @escaping (CLLocation?) -> Void) {
        Locator.currentPosition(accuracy: .city, onSuccess: { (loc) -> (Void) in
            location(loc)
        }) { (error, loc) -> (Void) in
            location(nil)
        }
    }
    
    func locationSetting() {
        if !CLLocationManager.locationServicesEnabled() {
            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
                // If general location settings are disabled then open general location settings
                UIApplication.shared.openURL(url)
            }
        } else {
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                // If general location settings are enabled then open location settings for the app
                UIApplication.shared.openURL(url)
            }
        }
    }
}
