//
//  FoodMenuPresenter.swift
//  Madhang
//
//  Created by asharijuang on 1/17/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol FoodMenuView : BaseView {
    func setData(value: [FoodViewData])
    func setDataGroup(value: [String : [FoodViewData]])
}

class FoodMenuPresenter {
    private let service : FoodService
    private var foodView : FoodMenuView?
    private var originalData : [FoodViewData] = [FoodViewData]()
    init() {
        self.service = FoodService()
    }
    
    func attachView(view : FoodMenuView){
        foodView = view
    }
    
    func detachView() {
        foodView = nil
    }
    
    func syncOrder() {
        let orderData = OrderManager.shared.sync(data: self.originalData)
        let group = self.menuGroup(menu: orderData)
        if !group.isEmpty {
            self.foodView?.setDataGroup(value: group)
        }
    }
    
    func loadData(shop: ShopViewData?) {
        self.foodView?.startLoading(message: "")
        if shop == nil {
            self.foodView?.setEmptyData(message: "Maaf ada kesalahan")
            return
        }
        service.loadFood(shopID: (shop?.id)!) { (response) in
            self.foodView?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    let data = result.map({ i in
                        return FoodViewData(id: i.id, shopID: (shop?.id)!, ownerID: (shop?.ownerID)!, imageUrl: i.imageUrl, name: i.name, category: i.category, desc: i.desc, price: i.price, time: i.time, status: i.status)
                    })
                    self.originalData = data
                    // sync with local order
                    let orderData = OrderManager.shared.sync(data: data)
                    let group = self.menuGroup(menu: orderData)
                    
                    self.foodView?.setDataGroup(value: group)
//                    self.foodView?.setData(value: orderData)
                }else {
                    self.foodView?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
            default:
                break
            }
        }
    }
    
    func menuGroup(menu : [FoodViewData]) -> [String : [FoodViewData]] {
        var group = [String : [FoodViewData]]()
        for g in menu {
            if group[g.category] == nil {
                // new
                group[g.category] = [g]
            }else {
                // exist and add qty
                group[g.category]?.append(g)
            }
        }
        return group
    }
    
    func menuCategory(menu : [String : [FoodViewData]]) -> [String] {
        let keys = Array(menu.keys)
        return keys
    }
    
    func clear() {
        OrderManager.shared.clear()
    }
}
