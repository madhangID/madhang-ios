//
//  OrderDetailPresenter.swift
//  Madhang
//
//  Created by asharijuang on 1/28/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import UIKit

protocol OrderDetailView : BaseView {
    func successCancelOrder(message: String)
}

class OrderDetailPresenter {
    private let service : OrderService
    private var viewPresenter : OrderDetailView?
    private var manager : OrderManager = OrderManager.shared
    
    init() {
        self.service = OrderService()
    }
    
    func attachView(view : OrderDetailView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func cancelOrder(id: String) {
        self.viewPresenter?.startLoading(message: "Mohon Tunggu")
        
        self.service.cancel(userId: Profile().id, orderId: id, completion: { (response) in
            self.viewPresenter?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result {
                    self.viewPresenter?.successCancelOrder(message: "Order dibatalkan")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
        })
    }
    
    func chat(id: String, target: UIViewController) {
        MainApp.shared.chat(withUser: id, target: target)
    }
}
