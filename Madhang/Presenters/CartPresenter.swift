//
//  CartPresenter.swift
//  Madhang
//
//  Created by asharijuang on 1/13/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol CartView : BaseView {
    func setData(value: [FoodViewData])
    func willUpdate()
    func startOrder(message: String)
    func finishOrder(message: String)
    func failedOrder(message: String)
    func startCoupon(message: String)
    func finishCoupon(message: String)
}

class CartPresenter {
    private let service : OrderService
    private var viewPresenter : CartView?
    private var manager : OrderManager = OrderManager.shared
    
    init() {
        self.service = OrderService()
        self.manager.update = { _ in
            self.viewPresenter?.willUpdate()
        }
    }
    
    func attachView(view : CartView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.startLoading(message: "")
        let order = self.manager.getOrderItem()
        if order.count > 0 {
            // convert order to view model
            var result = [FoodViewData]()
            for i in order {
                let data = FoodViewData(id: i.id, shopID: i.shopID, ownerID: i.shopOwnerID, imageUrl: i.imageUrl, name: i.name, category: i.category, desc: i.desc, price: i.price, time: i.time, status: i.status, total: i.total)
                result.append(data)
            }
            self.viewPresenter?.setData(value: result)
        }else {
            self.viewPresenter?.setEmptyData(message: "")
        }
        self.viewPresenter?.finishLoading(message: "")
    }
    
    func chechVoucher(code: String) {
        self.viewPresenter?.startCoupon(message: "Pengecekan code voucher")
        self.service.coupon(code: code, total: manager.totalPrice, userId: Profile().id) { (response) in
            self.viewPresenter?.finishCoupon(message: "")
            switch(response) {
            case .done(value: let coupon):
                self.manager.discount   = coupon.discount
                self.manager.voucher    = code
                self.viewPresenter?.finishCoupon(message: coupon.message)
                break
            case .failed(message: let message):
                self.viewPresenter?.failedOrder(message: message)
            default: break
            }
        }
        
    }
    
    func clear() {
        self.manager.clear()
    }
    
    func preccess(order: Order) {
        self.viewPresenter?.startOrder(message: "Mohon Tunggu, Pesanan anda sedang di validasi")
        let (validate,message) = self.manager.validate()
        if validate {
            let param = self.manager.getParams(order: order)
            self.service.order(param: param, completion: { (response) in
                switch(response){
                case .done(value: let result):
                    self.viewPresenter?.finishOrder(message: "Pesanan anda telah tersimpan")
                    self.manager.clear()
                    break
                case .failed(message: let message):
                    print("error message \(message)")
                    self.viewPresenter?.failedOrder(message: message)
                default:
                    break
                }
            })
        }else {
            self.viewPresenter?.failedOrder(message: message)
        }
    }
    
    func subTotal() -> String {
        return "Rp. \(manager.subTotal),-"
    }
    
    func total() -> String {
        return "Rp. \(manager.totalPrice),-"
    }
    
    func discount() -> String {
        return "Rp. \(manager.discount),-"
    }
    
    private func forTailingZero(temp: Double) -> String{
        let tempVar = String(format: "%g", temp)
        return tempVar
    }
}
