//
//  FoodSearchPresenter.swift
//  Madhang
//
//  Created by Qiscus on 10/11/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//
protocol BaseView {
    func startLoading(message: String)
    func finishLoading(message: String)
    func setEmptyData(message: String)
}

protocol FoodSearchView : BaseView {
    func setData(value: [ShopViewData])
}

class FoodSearchPresenter {
    private let foodService : FoodService
    private var foodView : FoodSearchView?
    
    init() {
        self.foodService = FoodService()
    }
    
    func attachView(view : FoodSearchView){
        foodView = view
    }
    
    func detachView() {
        foodView = nil
    }
    
    func loadData(type: String) {
        self.foodView?.startLoading(message: "")
        foodService.loadData(type: type) { (response) in
            self.foodView?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    let data = result.map({ food in
                        return  ShopViewData(id: food.id,ownerID: food.ownerID, name: food.name, rating: food.rating, review: food.review, address: food.address,addressCity: food.addressCity,addressProvince: food.addressProvince, open: food.open, imageUrl: food.imageUrl, distance: food.distance, note: food.note, phone: food.phone, isOpen: food.isOpen, max_visitor: food.max_visitor, lat: food.lat, lang: food.lang, fasility: food.fasility)
                    })
                    self.foodView?.setData(value: data)
                }else {
                    self.foodView?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
            default:
                break
            }
        }
    }
    
    func loadData(category: String) {
        self.foodView?.startLoading(message: "")
        foodService.loadData(category: category) { (response) in
            self.foodView?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    let data = result.map({ food in
                        return  ShopViewData(id: food.id,ownerID: food.ownerID, name: food.name, rating: food.rating, review: food.review, address: food.address,addressCity: food.addressCity,addressProvince: food.addressProvince, open: food.open, imageUrl: food.imageUrl, distance: food.distance, note: food.note, phone: food.phone, isOpen: food.isOpen, max_visitor: food.max_visitor, lat: food.lat, lang: food.lang, fasility: food.fasility)
                    })
                    self.foodView?.setData(value: data)
                }else {
                    self.foodView?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
            default:
                break
            }
        }
    }
    
    func loadData(keywords: String) {
        self.foodView?.startLoading(message: "")
        foodService.loadData(keywords: keywords) { (response) in
            self.foodView?.finishLoading(message: "")
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    
                    let data = result.map({ food in
                        return  ShopViewData(id: food.id,ownerID: food.ownerID, name: food.name, rating: food.rating, review: food.review, address: food.address,addressCity: food.addressCity, addressProvince: food.addressProvince, open: food.open, imageUrl: food.imageUrl, distance: food.distance, note: food.note, phone: food.phone, isOpen: food.isOpen, max_visitor: food.max_visitor, lat: food.lat, lang: food.lang, fasility: food.fasility)
                    })
                    self.foodView?.setData(value: data)
                }else {
                    self.foodView?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
            default:
                break
            }
            
        }
    }
    
    func loadFacilities() -> [Facility] {
        return MainApp.shared.getFacilities()
    }
}

