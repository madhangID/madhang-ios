//
//  CategoryPresenter.swift
//  Madhang
//
//  Created by Qiscus on 11/11/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

protocol FoodCategoryView : BaseView {
    func setData(value: [FoodCategoryViewData])
}

class FoodCategoryPresenter {
    private let service         : FoodCategoryService
    private var viewPresenter   : FoodCategoryView?
    
    init() {
        self.service = FoodCategoryService()
    }
    
    func attachView(view : FoodCategoryView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData() {
        self.viewPresenter?.startLoading(message: "")
        service.loadData { (response) in
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    let data = result.map({ result in
                        return FoodCategoryViewData(id: result.id, name: result.name, imageUrl: result.imageUrl)
                    })
                    self.viewPresenter?.setData(value: data)
                }else {
                    self.viewPresenter?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
            
            self.viewPresenter?.finishLoading(message: "")
            
            
        }
    }
}
