//
//  OrderPresenter.swift
//  Madhang
//
//  Created by asharijuang on 1/21/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

protocol OrderHistoryView : BaseView {
    func setOrder(status: OrderStatus,value: [OrderHistory])
    func willUpdate()
}

class OrderPresenter {
    
    private let service : OrderService
    private var viewPresenter : OrderHistoryView?
    private var manager : OrderManager = OrderManager.shared
    
    init() {
        self.service = OrderService()
        self.manager.update = { _ in
            self.viewPresenter?.willUpdate()
        }
    }
    
    func attachView(view : OrderHistoryView){
        viewPresenter = view
    }
    
    func detachView() {
        viewPresenter = nil
    }
    
    func loadData(status: OrderStatus = .done) {
        self.viewPresenter?.startLoading(message: "Mohon Tunggu")
        
        self.service.load(status: status, id: Profile().id, completion: { (response) in
            switch(response){
            case .done(value: let result):
                if result.count > 0 {
                    self.viewPresenter?.setOrder(status: status, value: result)
                }else {
                    self.viewPresenter?.setEmptyData(message: "")
                }
                break
            case .failed(message: let message):
                print("error message \(message)")
                self.viewPresenter?.setEmptyData(message: message)
            default:
                break
            }
            self.viewPresenter?.finishLoading(message: "")
        })
    }
}
