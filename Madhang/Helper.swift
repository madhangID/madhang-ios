//
//  Helper.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

class Style {
    static var sectionHeaderTitleFont = UIFont(name: "Helvetica-Bold", size: 20)
    static var navigationBarColor = UIColor.groupTableViewBackground
    static var navigationBarTitleColor = UIColor.orange
}

enum ENVIRONMENT : String {
    case Production
    case Staging
}

class Helper {
    static let DEMO_NUMBER = "6285727170251"
    // Sandbox
     static let BASE_URL = "https://sandbox.madhang.id/v1/"
     static let API_KEY  : String = "cf4f75861e4aef7d00e7b1dc6623812b16be84b9fb8a3e61bdcb7325d67153f0ea13832e99fab39a21b9939d5f643a3b669fe42d266f8a2fb604dc57d563e687"
    static let CHAT_APP_ID : String = "madhang-c-yf3kv7niqgg"

    // Prod
//    static let BASE_URL = "https://api.madhang.id/v1/"
//    static let API_KEY  : String = "54266bddde64820ce05f235a2b65ad302af40a57e25418f56a6195d4ab44977de7c5150d704bd79f0ca2ae38d7480c06bc4dab87a0bc57382c7790b6d73de36b"
//    static let CHAT_APP_ID : String = "madhang-c-yf3kv7niqgg"
    static let PAYMENT_ID : String = "SB-Mid-client-2eJ-e0TAweW-adkC"
    
    class func orange() -> UIColor {
        return UIColor.orange
    }
    class func black() -> UIColor {
        return UIColor(red: 38, green: 38, blue: 38, alpha: 1)
    }
    class func softGray() -> UIColor {
        return UIColor(red: 152, green: 156, blue: 160, alpha: 1)
    }
    class func boldGray() -> UIColor {
        return UIColor.darkGray
    }
    
    class func showLoading(message: String = "") {
        HUD.show(.labeledProgress(title: "", subtitle: message))
    }
    
    class func showError(message: String = "") {
        HUD.flash(.label(message), delay: 3.0)
    }
    
    class func showFlash(message : String) {
        HUD.flash(HUDContentType.labeledSuccess(title: "", subtitle: message), delay: 1.0)
    }
    
    class func dissmissLoading() {
        HUD.hide(animated: true)
    }
    
    class func showInputDialog(target: UIViewController,
                         title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        target.present(alert, animated: true, completion: nil)
    }
}
