//
//  Food.swift
//  Madhang
//
//  Created by asharijuang on 1/14/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON
/*
 "idmenu": "-L-Pz8rVmAjWyyKuKdlD",
 "nama": "Nasgor biasa",
 "category": "nasi goreng",
 "deskripsi": "Nasi Goreng Biasa",
 "foto": "http://sandbox.madhang.id/images/menu/61083daff58f9a5c94240b1b516a467d.jpeg",
 "harga": 22000,
 "time": "03/12/2017",
 "status": "biasa"
 */

class FoodViewData : NSObject {
    var id          : String    = ""
    var shopID      : String    = ""
    var shopOwnerID : String    = ""
    var imageUrl    : String    = "http://"
    var name        : String    = ""
    var category    : String    = ""
    var desc        : String    = ""
    var price       : Int       = 0
    var time        : String    = ""
    var status      : String    = ""
    var total       : Int       = 0
    
    init(id: String, shopID: String, ownerID: String, imageUrl: String, name: String, category: String, desc: String, price: Int, time: String, status: String, total: Int = 0) {
        self.id             = id
        self.shopID         = shopID
        self.shopOwnerID    = ownerID
        self.imageUrl       = imageUrl
        self.name           = name
        self.category       = category
        self.desc           = desc
        self.price          = price
        self.time           = time
        self.status         = status
        self.total          = total
    }
}

class Food : NSObject {
    var id          : String    = ""
    var shopID      : String    = ""
    var shopOwnerID : String    = ""
    var imageUrl    : String    = "http://"
    var name        : String    = ""
    var category    : String    = ""
    var desc        : String    = ""
    var price       : Int       = 0
    var time        : String    = ""
    var status      : String    = ""
    
    override init() { }
    
    init(json: JSON) {
        self.id         = json["idmenu"].string ?? ""
        self.name       = json["nama"].string ?? ""
        self.category   = json["category"].string ?? ""
        self.desc       = json["deskripsi"].string ?? ""
        self.imageUrl   = json["foto"].string ?? "http://"
        self.price      = json["harga"].int ?? 0
        self.time       = json["time"].string ?? ""
        self.status     = json["status"].string ?? ""
    }
}
