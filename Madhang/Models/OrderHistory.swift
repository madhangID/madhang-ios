//
//  OrderHistory.swift
//  Madhang
//
//  Created by asharijuang on 1/21/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

enum OrderHistoryStatus : String {
    case pending    = "Pending"
    case rejected   = "Ditolak"
    case accept     = "Diterima"
    case confirm    = "Konfirmasi"
    case canceled   = "Canceled"
    case finished   = "Selesai"
    case ready      = "Ready"
    case unknown    = "Unknown"
}

class OrderHistory {
    var shop    : Shop  = Shop()
    var order   : Order = Order()
    var status  : OrderHistoryStatus   = .pending
    var total   : Int    = 0
    var subTotal : Int  = 0
    var discount : Int  = 0
    var voucher : String = ""
    var userReview  : Bool  = false
    
    init(json: JSON) {
        let status = json["status_order"].string ?? "pending"
        if status == "pending" {
            self.status = .pending
        }else if status == "diterima" {
            self.status = .accept
        }else if status == "rejected" {
            self.status = .rejected
        }else if status == "canceled" {
            self.status = .canceled
        }else if status == "selesai" {
            self.status = .finished
        }else if status == "konfirmasi" {
            self.status = .confirm
        }else if status == "ready" {
            self.status = .ready
        }else {
            self.status = .unknown
        }
        self.userReview = json["ulasan_user"].bool ?? false
        
        
        self.order.id   = json["idorder"].string ?? "-"
        self.total        = json["total"].int ?? 0
        self.subTotal   = json["total_lama"].int ?? 0
        self.discount   = json["potongan"].int ?? 0
        self.voucher    = json["event_kupon"].string ?? "-"
        self.order.orderDate    = json["tanggal"].string ?? "01/01/2018"
        self.order.orderTime    = json["time"].string ?? "00.00"
        self.order.note         = json["catatan"].string ?? "-"
        self.shop.id    = json["tenantidtoko"].string ?? ""
        self.shop.name    = json["tenanttokonama"].string ?? ""
        self.shop.imageUrl    = json["tenanttokofoto"].string ?? "http://"
        self.shop.address    = json["tenanttokoalamat"].string ?? "http://"
        if let phone = json["tenanttokophone"].string {
            self.shop.phone = phone
        }
        if let phone = json["tenanttokophone"].int {
            self.shop.phone = String(phone)
        }
        self.order.items = [OrderItem]()
        let items = json["makanan"].arrayValue
        for item in items {
            let price = Int(item["harga"].string ?? "0") ?? 0
            let number  = item["jumlah"].string ?? "0"
            let data = OrderItem(id: item["idmenu"].string ?? "", name: item["nama"].string ?? "", price: price, total: Int(number) ?? 0, type: item["type"].string ?? "")
            self.order.items?.append(data)
        }
    }
}
