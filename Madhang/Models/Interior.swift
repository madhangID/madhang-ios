//
//  Interior.swift
//  Madhang
//
//  Created by asharijuang on 2/20/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON
/*
 {
 "keterangan" : "flof",
 "foto" : "http:\/\/sandbox.madhang.id\/images\/interior\/690b4155a12c64f1f3d8c193c68aa32e.jpeg",
 "time" : "20\/02\/2018",
 "idinterior" : "-L5nkbYGjcwj7uLFJ3wW"
 }
 */

class Interior {
    var id          : String
    var imageUrl    : String
    var note        : String
    var time        : String
    
    init(json: JSON) {
        self.id         = json["idinterior"].string ?? ""
        self.imageUrl   = json["foto"].string ?? "http://"
        self.note       = json["keterangan"].string ?? "-"
        self.time       = json["time"].string ?? "-"
    }
}
