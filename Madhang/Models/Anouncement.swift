//
//  Anouncement.swift
//  Madhang
//
//  Created by asharijuang on 3/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 {
 "idnotif": "-L0-7Tn6SMSoHkgaY2h-",
 "judul": "Peresemian Aplikasi Madhang",
 "gambar": "https://madhang.id/images/About.png",
 "link": "http://madhang.id/",
 "pesan": "Selamat datang di Aplikasi madhang\n",
 "status": "tenant",
 "tanggal": "10 Dec 2017",
 "timestamp": 1512904453909,
 "waktu": "6:14"
 }
 */

enum UserType : String {
    case tenant = "tenant"
    case user   = "user"
}

class Anouncement {
    var id          = ""
    var title       = ""
    var imageURL    = "http://"
    var link        = "http://"
    var message     = ""
    var status  : UserType = .user
    var dateString  = "1 Jan 2018"
    var timeString  = "0.00"
    
    init(json: JSON) {
        self.id         = json["idnotif"].string ?? ""
        self.title      = json["judul"].string ?? ""
        self.imageURL   = json["gambar"].string ?? "http://"
        self.dateString = json["tanggal"].string ?? "1 Jan 2018"
        self.timeString = json["waktu"].string ?? "00.00"
        self.message    = json["pesan"].string ?? "-"
        
        let status = json["status"].string ?? "user"
        if status == UserType.tenant.rawValue {
            self.status = .tenant
        }else {
            self.status = .user
        }
    }
}
