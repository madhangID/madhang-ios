//
//  Profile.swift
//  Madhang
//
//  Created by asharijuang on 1/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation

class ProfileViewData {
    var id          : String    = ""
    var demo        : Bool      = false
    var name        : String    = ""
    var phone       : String    = ""
    var email       : String    = ""
    var gender      : String    = ""
    var address     : String    = ""
    var city        : String    = ""
    var provinsi    : String    = ""
    var imageURL    : String    = ""
    var identityURL : String    = ""
    var identitySelfURL : String = ""
    var identityNo : String = ""
    var rekening    : String    = ""
    
    // Get from API
    var saldo   : Int = 0
    var credits : Int = 0
    var coin    : Int = 0
    
    var verified : Bool = false
}

class Profile {
    let store = UserDefaults()
    let prefix = "mdu_"
    var demo : Bool {
        set {
            store.set(newValue, forKey: key("demo") )
        }
        get {
            return store.bool(forKey: key("demo"))
        }
    }
    
    var id : String {
        set {
            store.set(newValue, forKey: key("id") )
        }
        get {
            return store.string(forKey: key("id")) ?? ""
        }
    }
    var name : String {
        set {
            store.set(newValue, forKey: key("name") )
        }
        get {
            return store.string(forKey: key("name")) ?? ""
        }
    }
    var phone : String {
        set {
            store.set(newValue, forKey: key("phone") )
        }
        get {
            return store.string(forKey: key("phone")) ?? ""
        }
    }
    var email : String {
        set {
            store.set(newValue, forKey: key("email") )
        }
        get {
            return store.string(forKey: key("email")) ?? ""
        }
    }
    var gender : String {
        set {
            store.set(newValue, forKey: key("gender") )
        }
        get {
            return store.string(forKey: key("gender")) ?? ""
        }
    }
    var address : String {
        set {
            store.set(newValue, forKey: key("address") )
        }
        get {
            return store.string(forKey: key("address")) ?? ""
        }
    }
    var city : String {
        set {
            store.set(newValue, forKey: key("city") )
        }
        get {
            return store.string(forKey: key("city")) ?? ""
        }
    }
    var provinsi : String {
        set {
            store.set(newValue, forKey: key("provinsi") )
        }
        get {
            return store.string(forKey: key("provinsi")) ?? ""
        }
    }
    var imageURL : String {
        set {
            store.set(newValue, forKey: key("imageURL") )
        }
        get {
            return store.string(forKey: key("imageURL")) ?? "http://"
        }
    }
    
    var identityURL : String {
        set {
            store.set(newValue, forKey: key("identityURL") )
        }
        get {
            return store.string(forKey: key("identityURL")) ?? "http://"
        }
    }
    
    var identitySelfURL : String {
        set {
            store.set(newValue, forKey: key("identitySelfURL") )
        }
        get {
            return store.string(forKey: key("identitySelfURL")) ?? "http://"
        }
    }
    
    var identityNo : String {
        set {
            store.set(newValue, forKey: key("identityNo") )
        }
        get {
            return store.string(forKey: key("identityNo")) ?? "http://"
        }
    }
    
    var rekening : String {
        set {
            store.set(newValue, forKey: key("rekening") )
        }
        get {
            return store.string(forKey: key("rekening")) ?? ""
        }
    }
    
    // Get from API
    var deposite : Int {
        set {
            store.set(newValue, forKey: key("deposite") )
        }
        get {
            return store.integer(forKey: key("deposite"))
        }
    }
    var credits : Int {
        set {
            store.set(newValue, forKey: key("credits") )
        }
        get {
            return store.integer(forKey: key("credits"))
        }
    }
    var coin : Int {
        set {
            store.set(newValue, forKey: key("coin") )
        }
        get {
            return store.integer(forKey: key("coin"))
        }
    }
    
    var verified : Bool {
        set {
            store.set(newValue, forKey: key("verified") )
        }
        get {
            return store.bool(forKey: key("verified"))
        }
    }
    
    func key(_ name: String) -> String {
        return prefix + name
    }
    
    func clear() {
        store.removeObject(forKey: key("id"))
        store.removeObject(forKey: key("name"))
        store.removeObject(forKey: key("phone"))
        store.removeObject(forKey: key("email"))
        store.removeObject(forKey: key("gender"))
        store.removeObject(forKey: key("address"))
        store.removeObject(forKey: key("city"))
        store.removeObject(forKey: key("provinsi"))
        store.removeObject(forKey: key("imageURL"))
        store.removeObject(forKey: key("identityURL"))
        store.removeObject(forKey: key("identitySelfURL"))
        store.removeObject(forKey: key("identityNo"))
        store.removeObject(forKey: key("rekening"))
        store.removeObject(forKey: key("deposite"))
        store.removeObject(forKey: key("credits"))
        store.removeObject(forKey: key("coin"))
        store.removeObject(forKey: key("verified"))
        store.removeObject(forKey: key("demo"))
    }
}
