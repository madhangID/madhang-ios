//
//  Review.swift
//  Madhang
//
//  Created by asharijuang on 2/19/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 {
 "idtoko": "-KysiOTdjb533usygoC2",
 "idorder": "MSI151236736161E",
 "pemesannama": "Odilio Sans",
 "pemesanfoto": "http://sandbox.madhang.id/images/user/-",
 "pemesanuserid": "187d18c01e2aa6954b089f45a1b1d61e",
 "foto": "http://sandbox.madhang.id/images/review/",
 "ulasanisi": "enak banget",
 "ulasanrating": "5.0",
 "timestamp": 1515257134,
 "jarak_review": "06 Januari 2018"
 }
 */

class Review {
    var id          : String = ""
    var idOrder     : String = ""
    var orderName   : String = ""
    var orderImage  : String = "http://"
    var imageUrl    : String = "http://"
    var content     : String = ""
    var rating      : Double = 0.0
    var date        : String = ""
    
    init(json: JSON) {
        self.id         = json["idtoko"].string ?? ""
        self.idOrder    = json["idorder"].string ?? ""
        self.orderName  = json["pemesannama"].string ?? ""
        self.orderImage = json["pemesanfoto"].string ?? ""
        self.imageUrl   = json["foto"].string ?? ""
        self.content    = json["ulasanisi"].string ?? ""
        self.rating     = Double(json["ulasanrating"].string ?? "0.0")!
        self.date       = json["jarak_review"].string ?? ""
    }
}

struct ShopReview {
    var shopId  : String
    var rating  : Double
    var reviews : [Review]
}
