//
//  Food.swift
//  Madhang
//
//  Created by qiscus on 29/10/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import SwiftyJSON

struct ShopViewData {
    var id          : String    = ""
    var ownerID     : String    = ""
    var name        : String    = ""
    var rating      : String    = ""
    var review      : Int       = 0
    var address     : String    = ""
    var addressCity     : String    = ""
    var addressProvince : String    = ""
    var open        : String    = ""
    var imageUrl    : String    = "http://"
    var distance    : String    = ""
    var note        : String    = ""
    var phone       : String    = ""
    var isOpen      : Bool      = false
    var max_visitor : Int       = 1
    var lat         : Double    = 0.0
    var lang        : Double    = 0.0
    var fasility    : [Facility]      = [Facility]()
}
//MARK: food is warung or shop
class Shop {
    var id          : String    = ""
    var ownerID     : String    = ""
    var name        : String    = ""
    var rating      : String    = ""
    var review      : Int       = 0
    var address     : String    = ""
    var addressCity     : String    = ""
    var addressProvince : String    = ""
    var open        : String    = ""
    var imageUrl    : String    = "http://"
    var distance    : String    = ""
    var note        : String    = ""
    var phone       : String    = ""
    var isOpen      : Bool      = false
    var max_visitor : Int       = 1
    var lat         : Double    = 0.0
    var lang        : Double    = 0.0
    var fasility    : [Facility]      = [Facility]()
    
    init() { }
    
    init(json: JSON) {
        print("shop : \(json)")
        self.id         = json["idtoko"].string ?? ""
        self.ownerID    = json["userid"].string ?? ""
        self.imageUrl   = json["logo_toko"].string ?? ""
        self.name       = json["nama_toko"].string ?? "Food Name"
        let buka        = json["jam_buka"].string ?? "00:00"
        let tutup       = json["jam_tutup"].string ?? "00:00"
        self.open       = "\(buka) - \(tutup)"
        self.address    = json["alamat"].string ?? "Jl. Arjuna No.26"
        self.note       = json["deskripsi_toko"].string ?? "-"
        self.lat        = json["latitude"].double ?? 0.0
        self.lang       = json["longitude"].double ?? 0.0
        self.isOpen     = json["status_buka"].bool ?? false
        self.phone      = json["notelp_toko"].string ?? "+62"
        self.distance   = json["jarak"].string ?? "1"
        self.addressCity        = json["kota"].string ?? "Semarang"
        self.addressProvince    = json["provinsi"].string ?? "Jawa Tengah"
        // extract facility, to many logic lol
        let dataFacility   = json["fasilitas"]
        print(dataFacility)
        let facilities = MainApp.shared.getFacilities()
        for t in facilities {
            if dataFacility[t.id].boolValue {
                self.fasility.append(t)
            }
        }
    }
}
