//
//  coupon.swift
//  Madhang
//
//  Created by asharijuang on 3/31/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
{
    "status": "Sukses",
    "message": "Kode Kupon Tersedia",
    "results": {
        "diskon": 18000,
        "total": 72000
    }
}
*/

class Coupon {
    var message     : String = ""
    var discount    : Int = 0
    var total       : Int = 0
    
    init(json: JSON) {
        let result = json["results"]
        self.total         = result["total"].int ?? 0
        self.discount      = result["diskon"].int ?? 0
        self.message    = json["message"].string ?? "_"
    }
}
