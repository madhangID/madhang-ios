//
//  Constant.swift
//  Madhang
//
//  Created by asharijuang on 12/23/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit

class Constant {

    static let column: CGFloat = 3
    
    static let minLineSpacing: CGFloat = 10.0
    static let minItemSpacing: CGFloat = 10.0
    
    static let offset: CGFloat = 5.0 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
}
