//
//  Order.swift
//  Madhang
//
//  Created by asharijuang on 1/7/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

enum Payment : String {
    case cash   = "cash"
    case dompet = "dompet"
}

enum Delivery : String {
    case cod    = "On The Spot"
    case send   = "Delivery"
}

struct OrderViewData {
    var id          : String        = ""
    var shopID      : String        = ""
    var shopOwnerID : String        = ""
    var items       : [OrderItem]?  = nil
    var orderDate   : String        = "1/1/2018"
    var orderTime   : String        = "09.00"
    var numberGuest : Int           = 1
    var note        : String        = ""
    var type        : String        = "biasa"
    var payment     : Payment       = .cash
    var delivery    : Delivery      = .cod
    var total       : Int           = 0
}

// MARK: Todo save items with order, currently not use
class Order : NSObject {
    var id          : String        = ""
    var shopID      : String        = ""
    var shopOwnerID : String        = ""
    var items       : [OrderItem]?  = nil
    var orderDate   : String        = ""
    var orderTime   : String        = "20.10"
    var numberGuest : Int           = 1
    var note        : String        = ""
    var type        : String        = "biasa"
    var payment     : Payment       = .cash
    var delivery    : Delivery      = .cod
    var total       : Int {
        get {
            return OrderManager.shared.totalPrice
        }
    }
    
    override init() {
        //
    }
    
    
    
//    func encode(with aCoder: NSCoder) {
//        //        items
//        aCoder.encode(shopID, forKey: "shopID")
//        aCoder.encode(shopOwnerID, forKey: "shopOwnerID")
//        aCoder.encode(items, forKey: "items")
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        shopID   = aDecoder.decodeObject(forKey: "shopID") as! String
//        shopOwnerID   = aDecoder.decodeObject(forKey: "shopOwnerID") as! String
//        items   = aDecoder.decodeObject(forKey: "items") as! [OrderItem]
//    }
}

class OrderItem : Food, NSCoding {
    
    var type    : String
    var total   : Int
    
    init(id: String, name: String, price: Int, total: Int = 0, type: String = "Biasa") {
        self.type   = type
        self.total  = total
        super.init()
        self.id     = id
        self.name   = name
        self.price  = price
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(total, forKey: "total")
    }
    
    required init?(coder aDecoder: NSCoder) {
        type    = aDecoder.decodeObject(forKey: "type") as! String
        total   = Int(aDecoder.decodeCInt(forKey: "total"))
        super.init()
        id      = aDecoder.decodeObject(forKey: "id") as! String
        name    = aDecoder.decodeObject(forKey: "name") as! String
        price   = Int(aDecoder.decodeCInt(forKey: "price"))
    }
    
    func toJSON() -> String {
        return "{\"nama\":\"\(name)\", \"harga\":\"\(String(price))\", \"type\":\"\(type)\", \"jumlah\":\"\(String(total))\"}"
    }
    
    func param() -> [String:String] {
        
        return ["makanan[\(id)]":toJSON()]
    }
}
