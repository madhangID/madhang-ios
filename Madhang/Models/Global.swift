//
//  Global.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import SwiftyJSON

struct City {
    var id : String = ""
    var name : String = ""
    var province : String = ""
    var type : String = ""
    var city_name : String = ""
    var postal_code : String = ""
}

struct Provice {
    var id : String = ""
    var name : String   = ""
}

class Facility : NSObject, NSCoding {
    var id      : String = ""
    var name    : String = ""
    var url     : String = ""

    init(json: JSON) {
        self.id     = json["idfasilitas"].string ?? ""
        self.url    = json["img"].string ?? ""
        self.name   = json["nama"].string ?? "Unknown"
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(url, forKey: "imageUrl")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id      = aDecoder.decodeObject(forKey: "id") as! String
        name    = aDecoder.decodeObject(forKey: "name") as! String
        url     = aDecoder.decodeObject(forKey: "imageUrl") as! String
    }
}
