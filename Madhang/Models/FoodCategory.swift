//
//  FoodCategory.swift
//  Madhang
//
//  Created by asharijuang on 1/2/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 {
 idkategori = k01;
 img = "http://sandbox.madhang.id/images/kategori/ic_chicken.png";
 nama = "Aneka Daging";
 }
*/

struct FoodCategoryViewData {
    var id          : String    = ""
    var name        : String    = ""
    var imageUrl    : String    = ""
}

class FoodCategory {
    var id          : String    = ""
    var name        : String    = ""
    var imageUrl    : String    = "http://"
    
    init(json: JSON) {
        self.id         = json["idkategori"].string ?? ""
        self.imageUrl   = json["img"].string ?? ""
        self.name       = json["nama"].string ?? "Food Name"
    }
}


