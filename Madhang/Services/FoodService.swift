//
//  FoodService.swift
//  Madhang
//
//  Created by Qiscus on 10/11/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FoodService: NSObject {

    func loadData(type: String, completion: @escaping (RequestResult<[Shop]>) -> Void) {
        Alamofire.request(EndPoint.warung(type: type), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Shop]()
                            for category in data {
                                result.append(Shop(json: category))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func loadData(category: String, completion: @escaping (RequestResult<[Shop]>) -> Void) {
        Alamofire.request(EndPoint.warung(category: category), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Shop]()
                            for category in data {
                                result.append(Shop(json: category))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func loadData(keywords: String, completion: @escaping (RequestResult<[Shop]>) -> Void) {
        Alamofire.request(EndPoint.warung(keywords: keywords), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Shop]()
                            for category in data {
                                result.append(Shop(json: category))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func loadFood(shopID: String, completion: @escaping (RequestResult<[Food]>) -> Void) {
        Alamofire.request(EndPoint.food(shopID: shopID), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    print("menu result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Food]()
                            for food in data {
                                result.append(Food(json: food))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
}
