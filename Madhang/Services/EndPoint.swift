//
//  EndPoint.swift
//  Madhang
//
//  Created by asharijuang on 1/1/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit

let BASEURL = Helper.BASE_URL

class EndPoint: NSObject {
    static var coupon = BASEURL + "/Reward/get_kupon/"
    
    class func currentVersion() -> String {
        return BASEURL + "notification/get_new_version/"
    }
    
    class func category() -> String {
        return BASEURL + "kategori/get_kategori"
    }
    
    class func warung(type: String = "") -> String {
        let (lat, lang) = MainApp.shared.getLocation()
        return BASEURL + "toko/get_toko/?latitude=\(lat)&longitude=\(lang)&type=" + type
    }
    
    class func warung(category: String) -> String {
        let (lat, lang) = MainApp.shared.getLocation()
        return BASEURL + "toko/get_toko/?latitude=\(lat)&longitude=\(lang)&kategori=" + category
    }
    
    class func warung(keywords: String) -> String {
        let (lat, lang) = MainApp.shared.getLocation()
        return BASEURL + "toko/get_toko/?latitude=\(lat)&longitude=\(lang)&condition=search&keyword=" + keywords
    }
    
    class func warungDetail(id: String, userID: String) -> String {
        let (lat, lang) = MainApp.shared.getLocation()
        return BASEURL + "/v1/toko/detaildapur/?latitude=\(lat)&idtoko=\(id)&condition=idtoko&longitude=\(lang)&userid=\(userID)"
    }
    
    class func warungReview(id: String) -> String {
        return BASEURL + "/review/get_reviewtoko/?idtoko=\(id)"
    }
    
    class func warungInterior(id: String) -> String {
        return BASEURL + "/interior/get_interior/?idtoko=\(id)"
    }
    
    // Profile
    class func login() -> String {
        return BASEURL + "user/login/"
    }
    
    class func newProfile() -> String {
        return BASEURL + "user/add_user/"
    }
    
    class func profile(id: String) -> String {
        return BASEURL + "user/get_user/?userid=\(id)"
    }
    
    class func updateProfile() -> String {
        return BASEURL + "user/update_user/"
    }
    
    // Order
    class func food(shopID: String,type: String = "") -> String {
        return BASEURL + "makanan/get_makanan/?idtoko=\(shopID)&status=\(type)"
    }
    
    class func order() -> String {
        return BASEURL + "order_madhang/add_order/"
    }
    
    class func cancelOrder() -> String {
        return BASEURL + "order_madhang/update_order/"
    }
    
    class func getOrder(type: OrderStatus, id: String) -> String {
        if type == .done {
            return BASEURL + "order_madhang/get_order/?pemesanuserid=\(id)&status=selesai"
        }else {
            return BASEURL + "order_madhang/get_order/?pemesanuserid=\(id)&status=proses"
        }
    }
    
    // Generic
    class func getCity() -> String {
        return BASEURL + "indonesiaku/get_city/"
    }
    
    class func getProvince() -> String {
        return BASEURL + "indonesiaku/get_province/"
    }
    
    class func getFacility() -> String {
        return BASEURL + "fasilitas/get_fasilitas/"
    }
    
    class func getNotification() -> String {
        return BASEURL + "utils/get_notifikasi/" // get
    }
    
    class func payment() -> String {
        return BASEURL + "/trans.php/charge"
        
        /*
 {"customer_details":{"billing_address":{"address":"Jalan Arjuna No. 36","city":"Semarang","country_code":"IDN","first_name":"juang","phone":"6285727170251","postal_code":"50131"},"email":"juang@qiscus.com","first_name":"juang","phone":"6285727170251","shipping_address":{"address":"Jalan Arjuna No. 36","city":"Semarang","country_code":"IDN","first_name":"juang","phone":"6285727170251","postal_code":"50131"}},"custom_field1":"2022c199cc800e5fd2e25919ef9c6a07","item_details":[{"id":"TP25000","name":"Topup Mangpi","price":25000,"quantity":1},{"id":"ADM","name":"Administrasi","price":5000,"quantity":1}],"transaction_details":{"gross_amount":30000,"order_id":"TPMDG20180301220555202"}}
         */
    }
    
    // Payment
    class func getTopUp(id: String) -> String {
        return BASEURL + "topup/get_topup/?userid=\(id)" // Get
    }
    
    class func reedemCode() -> String {
        return BASEURL + "/v1/reward/add_topup/"
        /*
 POST param uniquecode String, userid String
 */
    }
}

