//
//  ShopService.swift
//  Madhang
//
//  Created by asharijuang on 2/19/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Alamofire
import SwiftyJSON

class ShopService {
    
    func loadReview(id: String, completion: @escaping (RequestResult<ShopReview>) -> Void) {
        Alamofire.request(EndPoint.warungReview(id: id), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    print("shop review result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var reviews = [Review]()
                            for review in data {
                                reviews.append(Review(json: review))
                            }
                            let rating = Double(json["ratingtoko"].string ?? "0.0")!
                            let shopReview = ShopReview(shopId: id, rating: rating, reviews: reviews)
                            completion(RequestResult.done(shopReview))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func loadInterior(id: String, completion: @escaping (RequestResult<[Interior]>) -> Void) {
        Alamofire.request(EndPoint.warungInterior(id: id), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    print("shop Interior result \(String(describing: json))")
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var interiors = [Interior]()
                            for i in data {
                                interiors.append(Interior(json: i))
                            }

                            completion(RequestResult.done(interiors))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
}
