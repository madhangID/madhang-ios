//
//  OrderService.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum OrderStatus : String {
    case done       = "selesai"
    case onProgress = "proses"
}

class OrderService: NSObject {
    func order(param: [String:Any], completion: @escaping (RequestResult<Bool>) -> Void) {
        Alamofire.request(EndPoint.order(), method: .post, parameters: param, headers: nil).responseJSON { (response) in
            print("order param \(param), \nresult \(String(describing: response.result.value))")
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    let success = json["status"].string
                    if let result = success {
                        if result == "Success" {
                            completion(RequestResult.done(true))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func load(status: OrderStatus,id: String, completion: @escaping (RequestResult<[OrderHistory]>) -> Void) {
        print("url: \(EndPoint.getOrder(type: status, id: id))")
        Alamofire.request(EndPoint.getOrder(type: status, id: id), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    
                    print("order result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [OrderHistory]()
                            for food in data {
                                result.append(OrderHistory(json: food))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func cancel(userId: String, orderId: String, completion: @escaping (RequestResult<Bool>) -> Void) {
        let param = [
            "cancel_user" : true,
            "pemesanuserid" : userId,
            "idorder" : orderId,
            ] as [String : Any]
        Alamofire.request(EndPoint.cancelOrder(), method: .post, parameters: param, headers: nil).responseJSON { (response) in
            print("order param \(param), \nresult \(String(describing: response.result.value))")
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    let success = json["status"].string
                    if let result = success {
                        if result == "Success" {
                            completion(RequestResult.done(true))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }

    func coupon(code: String, total: Int, userId: String, completion: @escaping (RequestResult<Coupon>) -> Void) {
        let param = [
            "kode_kupon" : code,
            "userid" : userId,
            "total" : total,
            ] as [String : Any]
        Alamofire.request(EndPoint.coupon, method: .post, parameters: param, headers: nil).responseJSON { (response) in
            
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    let success = json["status"].string
                    let message = json["message"].string ?? ""
                    if let result = success {
                        if result.lowercased() != "error" {
                            completion(RequestResult.done(Coupon(json: json)))
                        }else {
                            completion(RequestResult.failed(message: message))
                        }
                    }
                    print("coupon \(param), \(json)")
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
}
