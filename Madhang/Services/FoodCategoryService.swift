//
//  CategoryService.swift
//  Madhang
//
//  Created by qiscus on 26/11/17.
//  Copyright © 2017 asharijuang. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FoodCategoryService {
    
    func loadData(completion: @escaping (RequestResult<[FoodCategory]>) -> Void) {
        Alamofire.request(EndPoint.category(), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
//                    print("category result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [FoodCategory]()
                            for category in data {
                                result.append(FoodCategory(json: category))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
}
