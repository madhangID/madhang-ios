//
//  ProfileService.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileService: NSObject {

    func checkProfile(phone: String, completion: @escaping (RequestResult<(Bool)>) -> Void) {
        let param : [String : Any] = [
            "idlogin"   : phone,
            "type"      : "phone"
            ]
        
        Alamofire.request(EndPoint.login(), method: .post, parameters: param, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    print("profile result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            for profile in data {
                                let isComplete  = json["profile"].bool ?? false
                                let userId      = profile["userid"].string ?? ""
                                let name        = profile["nama"].string ?? ""
                                let email       = profile["email"].string ?? ""
                                let provinsi    = profile["provinsi"].string ?? ""
                                let city        = profile["kota"].string ?? ""
                                let address     = profile["alamat"].string ?? ""
                                let verified    = profile["verified"].bool ?? false
                                let imageUrl    = profile["foto"].string ?? "http://"
                                let ktp    = profile["ktp"].string ?? "http://"
                                
                                // Save user id to database
                                let p = Profile()
                                p.id        = userId
                                p.phone     = phone
                                p.address   = address
                                p.name      = name
                                p.email     = email
                                p.provinsi  = provinsi
                                p.city      = city
                                p.verified  = verified
                                p.imageURL  = imageUrl
                                p.identityURL   = ktp
                                completion(RequestResult.done(isComplete))
                            }
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func register(data: ProfileViewData, completion: @escaping (RequestResult<ProfileViewData>) -> Void) {
        let param : [String : Any] = [
            "userid"    : data.id,
            "nama"      : data.name,
            "email"     : data.email,
            "notelp"    : data.phone,
            "alamat"    : data.address,
            "kota"      : data.city,
            "provinsi"  : data.provinsi,
            "type"      : "phone"
        ]
        
        Alamofire.request(EndPoint.newProfile(), method: .post, parameters: param, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
//                    print("category result \(String(describing: response.result.value))")
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = ProfileViewData()
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }else {
                        let message = json["message"].string ?? "Error parsing data"
                        completion(RequestResult.failed(message: message))
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func update(data: ProfileViewData, completion: @escaping (RequestResult<ProfileViewData>) -> Void) {
        let param : [String : Any] = [
            "userid"    : data.id,
            "nama"      : data.name,
            "email"     : data.email,
            "notelp"    : data.phone,
            "alamat"    : data.address,
            "kota"      : data.city,
            "provinsi"  : data.provinsi,
            ]
        
        Alamofire.request(EndPoint.updateProfile(), method: .post, parameters: param, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let status = json["status"].string {
                        if status == "Success" {
                            //MARK: response just success or failed, not return object
                            completion(RequestResult.done(data))
                        }else {
                            let message = json["message"].string ?? "Error parsing data"
                            completion(RequestResult.failed(message: message))
                        }
                    }else {
                        let message = json["message"].string ?? "Error parsing data"
                        completion(RequestResult.failed(message: message))
                    }
                }else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func updateAvatar() {
    }
    
    func changeKTP(no: String, ktp: String, ktpSelf: String , completion: @escaping (RequestResult<(String,String)>) -> Void) {
        
        let parameters = [
            "userid": Profile().id,
            "no_ktp": no,
        ]
        let url = EndPoint.updateProfile()
        Alamofire.upload(multipartFormData:
            { formData in
                let fileKtpUrl = URL(fileURLWithPath: ktp)
                formData.append(fileKtpUrl, withName: "ktp")
                let fileKtpSelf = URL(fileURLWithPath: ktpSelf)
                formData.append(fileKtpSelf, withName: "ktp_diri")
                for (key, value) in parameters {
                    formData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
                         usingThreshold:UInt64.init(),
                         to: url,
                         headers: nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress(closure: { response in
                                    print("ktp progress \(response)")
                                    completion(RequestResult.onProgress(progress: response.fractionCompleted))

                                })
                                upload.responseJSON { response in
                                    print("ApiProfile update ktp\n: \(response)")
                                    if response.result.value != nil {
                                        if (response.response?.statusCode == 200) {
                                            print("update ktp \(String(describing: response.result.value))")
                                            let json = JSON(response.result.value!)
                                            let ktp = json["url_ktp"].string ?? ""
                                            let ktpSelf = json["url_ktp_diri"].string ?? ""
                                            if !ktp.isEmpty && !ktpSelf.isEmpty {
                                                completion(RequestResult.done((ktp,ktpSelf)))
                                            }else {
                                                completion(RequestResult.failed(message: "Failed to upload"))
                                            }
                                        }else {
                                            completion(RequestResult.failed(message: "Failed to upload"))
                                        }
                                    } else {
                                        completion(RequestResult.failed(message: "Failed to upload"))
                                    }
                                }
                            case .failure(let encodingError):
                                completion(RequestResult.failed(message: encodingError.localizedDescription))                          }
        })
    }
    
}
