//
//  ToolService.swift
//  Madhang
//
//  Created by asharijuang on 1/11/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ToolService {
    func facilities( completion: @escaping (RequestResult<[Facility]>) -> Void) {
        Alamofire.request(EndPoint.getFacility(), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Facility]()
                            for food in data {
                                result.append(Facility(json: food))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
    
    func anouncement( completion: @escaping (RequestResult<[Anouncement]>) -> Void) {
        Alamofire.request(EndPoint.getNotification(), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    if let data = json["results"].array {
                        if data.count >= 0 {
                            var result = [Anouncement]()
                            for anoun in data {
                                result.append(Anouncement(json: anoun))
                            }
                            completion(RequestResult.done(result))
                        }else {
                            completion(RequestResult.failed(message: "no data"))
                        }
                    }
                }else if (response.response?.statusCode)! >= 300 {
                    completion(RequestResult.failed(message: "error"))
                } else {
                    completion(RequestResult.failed(message: "error"))
                }
                
            } else {
                completion(RequestResult.failed(message: "Something went wrong"))
            }
        }
    }
}
