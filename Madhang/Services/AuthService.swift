//
//  AuthService.swift
//  Madhang
//
//  Created by asharijuang on 1/14/18.
//  Copyright © 2018 asharijuang. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    // get phone from graph facebook
    func getPhone(accessToken: String, completion: @escaping (RequestResult<(String)>) -> Void) {
        Alamofire.request("https://graph.accountkit.com/v1.2/me/?access_token=\(accessToken)", method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.value != nil {
                let json = JSON(response.result.value!)
                print("graph: \(json)")
                if (response.response?.statusCode == 200) {
                    let json = JSON(response.result.value!)
                    let phone = json["phone"]["number"].string ?? ""
                    let profile = Profile()
                    profile.phone   = phone
                    profile.demo    = false
                    completion(RequestResult.done(phone))
                }else{
                    completion(RequestResult.failed(message: "User Not Found"))
                }
            }
        }
    }
}
